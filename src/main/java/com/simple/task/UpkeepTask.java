package com.simple.task;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KeepRecord;
import com.simple.ding.DingUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UpkeepTask implements Runnable{
    /**
     * 保养到期前三天通知(之前选择的通知人,班长)
     * */
    @Override
    public void run() {
        Date nowDate=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:ss:mm");
        try {
            //查询所有在使用工装
            String sqlReceive="select a.*,c.barcode,c.frock_name,c.keep_days,c.inform_user_id from receive_back a,frocks c  "
                    + "where a.status=1  and a.frock_id=c.id";
            List<Record> receiveList= Db.use("dc").find(sqlReceive);
            for (int i = 0; i < receiveList.size(); i++) {
                /*//先将字符串前后的空格给去掉
                String dbInformUserIds = receiveList.get(i).getStr("inform_user_id");
                dbInformUserIds=dbInformUserIds.trim();  //去掉字符串前后空格
                //将通知人id字符串拆分（用空格拆分）
                String[] informUserIds=dbInformUserIds.split(" ");*/

                int keep_days=receiveList.get(i).getInt("keep_days");
                Date receiveDate=receiveList.get(i).getDate("receive_date");//领用日期
                long receiveDays=(nowDate.getTime()-receiveDate.getTime())/(24*60*60*1000);   //至今领用了多少天
                if (receiveDays>=keep_days-2) {//领用天数超过保养周期（到期前三天）
                    //判断是否有保养记录
                    List<KeepRecord> keepRecords=KeepRecord.dao.find("select * from keep_record "
                            + "where frock_id="+receiveList.get(i).getLong("frock_id")+" and status=0 ");
                    if (keepRecords.isEmpty()) {
                        //没有保养记录，直接提醒保养
                        //进行循环通知
                       /* for(String informUserId:informUserIds){
                            DingUtil.sendText(informUserId,
                                    "工装保养通知：\n员工 " + receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n未保养，将在3天内到达保养周期，请知悉！\n"+sdf.format(nowDate));
                        }*/
                        if(receiveList.get(i).getStr("inform_user_id")!=null){
                            DingUtil.sendText(receiveList.get(i).getStr("inform_user_id").replace(" ",","),
                                    "工装保养通知：\n员工 " + receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n未保养，将在3天内到达保养周期，请知悉！\n"+sdf.format(nowDate));
                        }

                    }else {//有保养记录
                        Date lastKeepDate=keepRecords.get(0).getKeepDate();
                        if (lastKeepDate.before(receiveDate)) {
                            //上个保养日期不在领用期间内，直接提前三天提醒通知人
                            if(receiveList.get(i).getStr("inform_user_id")!=null){
                                DingUtil.sendText(receiveList.get(i).getStr("inform_user_id").replace(" ",","),
                                        "工装保养通知：\n员工 " + receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n未保养，将在3天内到达保养周期，请知悉！\n"+sdf.format(nowDate));

                            }
                        }else {//上个保养日期在领用期间内,判断保养日期到现在是否超过保养天数前三天
                            long lastKeepDays=(nowDate.getTime()-lastKeepDate.getTime())/(24*60*60*1000);
                            if (lastKeepDays>=keep_days-3) {
                                //上个保养日期到现在 已超过保养周期结束的前三天，提醒
                                if(receiveList.get(i).getStr("inform_user_id")!=null){
                                    DingUtil.sendText(receiveList.get(i).getStr("inform_user_id").replace(" ",","),
                                            "工装保养通知：\n员工 " + receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n未保养，将在3天内到达保养周期，请知悉！\n"+sdf.format(nowDate));

                                }

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
