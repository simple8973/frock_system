package com.simple.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.ding.DingUtil;

public class ExpireTask implements Runnable{
	/**
	 * 提前一周提醒校验
	 * 库管理员，使用者，使用者的班长（通知人）
	 */
	@Override
	public void run(){
		Calendar date=Calendar.getInstance();
		date.add(Calendar.DATE, 7);
		String oneWeek=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date.getTime());
		Date nowDate=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			String sqlFrock="select * from frocks where next_check_date<='"+oneWeek+"' and status=0";
			List<Frocks> frocks=Frocks.dao.find(sqlFrock);
			for (int i = 0; i < frocks.size(); i++) {
		/*		//先将字符串前后的空格给去掉，这样会有字符串为空的时候，会报错
				String dbInformUserIds = frocks.get(i).getStr("inform_user_id");
				dbInformUserIds=dbInformUserIds.trim();  //去掉字符串前后空格
				//将通知人id字符串拆分（用空格拆分）
				String[] informUserIds=dbInformUserIds.split(" ");   //得到一个数组*/

				//判断是否在使用中
				List<ReceiveBack> receives=ReceiveBack.dao.find("select * from receive_back where frock_id="+frocks.get(i).getId()+" and status=1 ");
				//30512532441289105
				if (receives.isEmpty()) {
					if(frocks.get(i).getCreateUserId()!=null){
						DingUtil.sendText(frocks.get(i).getCreateUserId(), "工装校验通知：\n工装编号【"+frocks.get(i).getBarcode()+"】，工装名称："+frocks.get(i).getFrockName()+"，\n即将到期，请及时校验！\n"+sdf.format(nowDate));
					}
				}else {
					//钉钉推送可以推送由逗号隔开的字符串
					if (frocks.get(i).getStr("inform_user_id") != null) {
						DingUtil.sendText(frocks.get(i).getStr("inform_user_id").replace(" ",","), "工装校验通知：\n员工 "+receives.get(0).getReceiveUserName()+"领用的\n工装编号【"+frocks.get(i).getBarcode()+"】，工装名称："+frocks.get(i).getFrockName()+"，\n即将到期，请及时提醒归还！\n"+sdf.format(nowDate));
					}
					if(receives.get(0).getReceiveUserId()!=null){
						DingUtil.sendText(receives.get(0).getReceiveUserId(), "工装校验通知：\n工装编号【"+frocks.get(i).getBarcode()+"】，工装名称："+frocks.get(i).getFrockName()+"，\n即将到期，请及时归还校验！\n"+sdf.format(nowDate));
					}
					if(frocks.get(i).getCreateUserId()!=null){
						DingUtil.sendText(frocks.get(i).getCreateUserId(), "工装校验通知：\n工装编号【"+frocks.get(i).getBarcode()+"】，工装名称："+frocks.get(i).getFrockName()+"，\n即将到期，请及时回收校验！\n"+sdf.format(nowDate));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
