package com.simple.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KeepRecord;
import com.simple.ding.DingUtil;

public class ExpireTask2 implements Runnable{
	/**
	 * 7天之后提醒保养
	 */
	@Override
	public void run(){
		Date nowDate=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:ss:mm");
		try {
			//查询所有在使用工装
			String sqlReceive="select a.*,c.barcode,c.frock_name,c.keep_days,c.create_user_id from receive_back a,frocks c  "
					+ "where a.status=1  and a.frock_id=c.id";
			List<Record> receiveList=Db.use("dc").find(sqlReceive);
			for (int i = 0; i < receiveList.size(); i++) {
				int keep_days=receiveList.get(i).getInt("keep_days");
				Date receiveDate=receiveList.get(i).getDate("receive_date");//领用日期
				long receiveDays=(nowDate.getTime()-receiveDate.getTime())/(24*60*60*1000);
				if (receiveDays>=keep_days-2) {//还有三天到保养周期期限
					//判断是否有保养记录
					List<KeepRecord> keepRecords=KeepRecord.dao.find("select * from keep_record "
							+ "where frock_id="+receiveList.get(i).getLong("frock_id")+" and status=0 ");//keep_user_id
					if (keepRecords.isEmpty()) {
						//没有保养记录，直接提醒保养
						DingUtil.sendText(receiveList.get(i).getStr("receive_user_id"), "工装保养通知：\n工装编号【"+receiveList.get(i).getStr("barcode")+"】，工装名称："+receiveList.get(i).getStr("frock_name")+"，\n已超过"+keep_days+"天未保养，请及时保养！\n"+sdf.format(nowDate));
						//没有保养记录，且超过保养周期3天，提醒管理员
						if (receiveDays-keep_days>=3) {
							DingUtil.sendText(receiveList.get(i).getStr("create_user_id"), 
								"工装保养通知：\n员工 "+receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n已超过保养周期3天未保养，请知悉！\n"+sdf.format(nowDate));
						}
					}else {//有保养记录
						Date lastKeepDate=keepRecords.get(0).getKeepDate();
						if (lastKeepDate.before(receiveDate)) {
							//上个保养日期不在领用期间内，直接提醒
							DingUtil.sendText(receiveList.get(i).getStr("receive_user_id"),"工装保养通知：\n工装编号【"+receiveList.get(i).getStr("barcode")+"】，工装名称："+receiveList.get(i).getStr("frock_name")+"，\n已超过"+keep_days+"天未保养，请及时保养！\n"+sdf.format(nowDate));
							//已超保养周期3天未保养 提醒管理员
							if (receiveDays-keep_days>=3) {
								DingUtil.sendText(receiveList.get(i).getStr("create_user_id"), 
									"工装保养通知：\n员工 "+receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n已超过保养周期3天未保养，请知悉！\n"+sdf.format(nowDate));
							}
						}else {//上个保养日期在领用期间内,判断保养日期到现在是否超过保养天数
							long lastKeepDays=(nowDate.getTime()-lastKeepDate.getTime())/(24*60*60*1000);
							if (lastKeepDays>=keep_days-2) {   //这也改成了提前三天
								//上个保养日期到现在 已超过保养周期，提醒
								DingUtil.sendText(receiveList.get(i).getStr("receive_user_id"), "工装保养通知：\n工装编号【"+receiveList.get(i).getStr("barcode")+"】，工装名称："+receiveList.get(i).getStr("frock_name")+"，\n已超过"+keep_days+"天未保养，请及时保养！\n"+sdf.format(nowDate));
								//已超保养周期3天未保养 提醒管理员
								if (lastKeepDays-keep_days>=3) {
									DingUtil.sendText(receiveList.get(i).getStr("create_user_id"), 
										"工装保养通知：\n员工 "+receiveList.get(i).getStr("receive_user_name")+"领用的 \n工装编号【"+receiveList.get(i).getStr("barcode")+"】\n工装名称："+receiveList.get(i).getStr("frock_name")+"\n已超过保养周期3天未保养，请知悉！\n"+sdf.format(nowDate));
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
