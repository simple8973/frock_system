package com.simple.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.simple.common.model.KMaterialList;

/**
 * 合格工装自动确认
 * @author FL00024996
 *
 */
public class QualifyTask implements Runnable{
	@Override
	public void run(){
		try {
			Calendar date=Calendar.getInstance();
			date.add(Calendar.DATE, -30);
			String oneMonth=new SimpleDateFormat("yyyy-MM-dd").format(date.getTime())+" 00:00:00"; 
			List<KMaterialList> materialLists=KMaterialList.dao.find("select * from k_material_list where status=0 and receive_date<='"+oneMonth+"'");
			for (int i = 0; i < materialLists.size(); i++) {
				materialLists.get(i).setIsQualified(1).setStatus(1).setSureDate(new Date());
				materialLists.get(i).update();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
