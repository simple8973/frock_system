package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.app.AppBackController;
import com.simple.controller.app.AppCheckController;
import com.simple.controller.app.AppController;
import com.simple.controller.app.AppFrockController;
import com.simple.controller.app.AppKeepController;
import com.simple.controller.app.AppLingYongController;
import com.simple.controller.app.AppReceiveGoodsController;
import com.simple.controller.app.AppRepairController;

public class AppRoutes extends Routes{
	@Override
	public void config() {
		this.add("/app",AppController.class);
		this.add("frockInfo",AppFrockController.class);
		this.add("appLingyong",AppLingYongController.class);//app领用
		this.add("appBack",AppBackController.class);//app归还
		this.add("appCheck",AppCheckController.class);//app校验
		this.add("appKeep",AppKeepController.class);//app保养
		this.add("appRepair",AppRepairController.class);//工装报修
		this.add("appReceiveGoods",AppReceiveGoodsController.class);//扫码收获
	}
}
