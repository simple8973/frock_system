package com.simple.common.config;

public class URLConstant {
	public static final String URL_GET_TOKEN="https://oapi.dingtalk.com/gettoken";
	
	public static final String URL_GET_USER_INFO="https://oapi.dingtalk.com/user/getuserinfo";
	
	public static final String URL_USER_GET="https://oapi.dingtalk.com/user/get";
	
	public static final String URL_MESSAGE_ASYNCSEND_V2="https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2";
}
