package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.StockPart.ErpStockPartController;
import com.simple.controller.StockPart.StockPartController;
import com.simple.controller.frock.CheckFrockController;
import com.simple.controller.frock.FrockController;
import com.simple.controller.frock.ReceiveController;
import com.simple.controller.frock.RepairController;
import com.simple.controller.frock.WareHouseController;
import com.simple.controller.frock.frockKeepController;
import com.simple.controller.fujian.FujianController;
import com.simple.controller.receive.MoveFrockController;
import com.simple.controller.receive.QualifiedFrockController;
import com.simple.controller.receive.ReceiveFrockController;

public class PcRoutes extends Routes{
	@Override
	public void config() {
		this.setBaseViewPath("/page");
		this.add("frock",FrockController.class,"/frock");//工装
		this.add("receive",ReceiveController.class,"/frock");//领用归还
		this.add("repaire",RepairController.class,"/frock");//报修
		this.add("fujian",FujianController.class);
		this.add("check",CheckFrockController.class,"/frock");//工装校验
		this.add("keepPc",frockKeepController.class,"/frock");//保养记录
		this.add("wareHouse",WareHouseController.class,"/frock");//车间
		
		this.add("receiveFrock",ReceiveFrockController.class,"/receive");//收获列表
		this.add("qualifyInstore",QualifiedFrockController.class,"/receive");//合格品入库
		this.add("frockMove",MoveFrockController.class,"/receive");//工装移库
		
		this.add("stockPart",StockPartController.class,"/stockPart");//库存件
		this.add("erpStockPart",ErpStockPartController.class,"/stockPart");//库存件
	}

}
