package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.MainController;
import com.simple.controller.system.DingLoginController;
import com.simple.controller.system.OsController;
import com.simple.controller.system.SystemController;

public class AdminRoutes extends Routes {

	@Override
	public void config() {
		this.setBaseViewPath("/page");
		this.add("/",MainController.class);
		this.add("/dl",DingLoginController.class);
		this.add("/sys",SystemController.class); //角色设置、用户设置角色
		this.add("/os",OsController.class,"sys");
	}

}