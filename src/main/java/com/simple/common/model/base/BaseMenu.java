package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseMenu<M extends BaseMenu<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public M setMenuName(java.lang.String menuName) {
		set("menu_name", menuName);
		return (M)this;
	}
	
	public java.lang.String getMenuName() {
		return getStr("menu_name");
	}

	public M setMenuLevel(java.lang.Integer menuLevel) {
		set("menu_level", menuLevel);
		return (M)this;
	}
	
	public java.lang.Integer getMenuLevel() {
		return getInt("menu_level");
	}

	public M setMenuPid(java.lang.Integer menuPid) {
		set("menu_pid", menuPid);
		return (M)this;
	}
	
	public java.lang.Integer getMenuPid() {
		return getInt("menu_pid");
	}

	public M setMenuTitleEn(java.lang.String menuTitleEn) {
		set("menu_title_en", menuTitleEn);
		return (M)this;
	}
	
	public java.lang.String getMenuTitleEn() {
		return getStr("menu_title_en");
	}

	public M setHref(java.lang.String href) {
		set("href", href);
		return (M)this;
	}
	
	public java.lang.String getHref() {
		return getStr("href");
	}

	public M setIsHide(java.lang.Integer isHide) {
		set("is_hide", isHide);
		return (M)this;
	}
	
	public java.lang.Integer getIsHide() {
		return getInt("is_hide");
	}

	public M setSeqNum(java.lang.Integer seqNum) {
		set("seq_num", seqNum);
		return (M)this;
	}
	
	public java.lang.Integer getSeqNum() {
		return getInt("seq_num");
	}

	public M setIcon(java.lang.String icon) {
		set("icon", icon);
		return (M)this;
	}
	
	public java.lang.String getIcon() {
		return getStr("icon");
	}

	public M setType(java.lang.Integer type) {
		set("type", type);
		return (M)this;
	}
	
	public java.lang.Integer getType() {
		return getInt("type");
	}

}

