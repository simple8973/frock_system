package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseWareHouseRecord<M extends BaseWareHouseRecord<M>> extends Model<M> implements IBean {

	/**
	 * 车间操作记录
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 车间操作记录
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 车间id
	 */
	public M setWareId(java.lang.Integer wareId) {
		set("ware_id", wareId);
		return (M)this;
	}
	
	/**
	 * 车间id
	 */
	public java.lang.Integer getWareId() {
		return getInt("ware_id");
	}

	public M setUserId(java.lang.String userId) {
		set("user_id", userId);
		return (M)this;
	}
	
	public java.lang.String getUserId() {
		return getStr("user_id");
	}

	/**
	 * 操作内容
	 */
	public M setContent(java.lang.String content) {
		set("content", content);
		return (M)this;
	}
	
	/**
	 * 操作内容
	 */
	public java.lang.String getContent() {
		return getStr("content");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

}

