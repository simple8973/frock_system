package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 公司及关联域
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseKCompanyField<M extends BaseKCompanyField<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 公司名称
	 */
	public M setCompanyName(java.lang.String companyName) {
		set("company_name", companyName);
		return (M)this;
	}
	
	/**
	 * 公司名称
	 */
	public java.lang.String getCompanyName() {
		return getStr("company_name");
	}

	/**
	 * 公司编码
	 */
	public M setCompanyCode(java.lang.String companyCode) {
		set("company_code", companyCode);
		return (M)this;
	}
	
	/**
	 * 公司编码
	 */
	public java.lang.String getCompanyCode() {
		return getStr("company_code");
	}

	/**
	 * 域
	 */
	public M setField(java.lang.String field) {
		set("field", field);
		return (M)this;
	}
	
	/**
	 * 域
	 */
	public java.lang.String getField() {
		return getStr("field");
	}

}

