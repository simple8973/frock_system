package com.simple.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseFrockRecord<M extends BaseFrockRecord<M>> extends Model<M> implements IBean {

	/**
	 * 工装记录表
	 */
	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 工装记录表
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 工装id
	 */
	public M setFrockId(java.lang.Long frockId) {
		set("frock_id", frockId);
		return (M)this;
	}
	
	/**
	 * 工装id
	 */
	public java.lang.Long getFrockId() {
		return getLong("frock_id");
	}

	public M setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
		return (M)this;
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	/**
	 * 1-完成
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 1-完成
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

}
