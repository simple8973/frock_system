package com.simple.common.model;

import com.simple.common.model.base.BaseFrockEditRecord;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class FrockEditRecord extends BaseFrockEditRecord<FrockEditRecord> {
	public static final FrockEditRecord dao = new FrockEditRecord().dao();
}
