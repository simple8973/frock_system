package com.simple.common.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JBolt, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("check_record", "id", CheckRecord.class);
		arp.addMapping("frock_edit_record", "id", FrockEditRecord.class);
		arp.addMapping("frock_repair", "id", FrockRepair.class);
		arp.addMapping("frocks", "id", Frocks.class);
		arp.addMapping("fujians", "id", Fujians.class);
		arp.addMapping("in_account", "id", InAccount.class);
		arp.addMapping("in_location", "id", InLocation.class);
		arp.addMapping("in_product", "id", InProduct.class);
		arp.addMapping("in_stock_part", "id", InStockPart.class);
		arp.addMapping("in_units", "id", InUnits.class);
		arp.addMapping("k_company_field", "id", KCompanyField.class);
		arp.addMapping("k_delivery", "id", KDelivery.class);
		arp.addMapping("k_erp_records", "id", KErpRecords.class);
		arp.addMapping("k_material_list", "id", KMaterialList.class);
		arp.addMapping("k_pallet_list", "id", KPalletList.class);
		arp.addMapping("keep_record", "id", KeepRecord.class);
		arp.addMapping("menu", "id", Menu.class);
		arp.addMapping("menu_permission", "id", MenuPermission.class);
		arp.addMapping("receive_back", "id", ReceiveBack.class);
		arp.addMapping("roles", "id", Roles.class);
		arp.addMapping("user_role", "id", UserRole.class);
		arp.addMapping("user_ware", "id", UserWare.class);
		arp.addMapping("ware_house", "id", WareHouse.class);
		arp.addMapping("ware_house_record", "id", WareHouseRecord.class);
	}
}


