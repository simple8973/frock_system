package com.simple.controller.receive;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KDelivery;
import com.simple.common.model.KErpRecords;
import com.simple.common.model.KMaterialList;
import oracle.jdbc.OracleTypes;
/**
 * 合格品入库 qualifyInstore/toQualityList
 * @author FL00024996
 *
 */
public class QualifiedFrockController extends Controller {
	public void toQualityList() {
		render("qualityList.html");
	}
	public void getQualifyList() {
		String deliveryNo=get("deliveryNo");
		String supplierName=get("supplierName");
		String orderNo=get("orderNo");
		String partNo=get("partNo");
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from k_delivery a,k_material_list b where a.id=b.delivery_id and (b.status=1 or b.status=4) ");
		if (!"".equals(deliveryNo)&&deliveryNo!=null) {
			sb.append(" and a.delivery_no like '%"+deliveryNo+"%' ");
		}
		if (!"".equals(supplierName)&&supplierName!=null) {
			sb.append(" and a.supplier_name like '%"+supplierName+"%' ");
		}
		if (!"".equals(orderNo)&&orderNo!=null) {
			sb.append(" and b.order_no like '%"+orderNo+"%' ");
		}
		if (!"".equals(partNo)&&partNo!=null) {
			sb.append(" and b.part_no like '%"+partNo+"%' ");
		}
		Page<Record> daojuList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.delivery_no,a.supplier_name,a.deliver_lbn ",sb.toString()+" order by b.status asc");
		Record jsp=new Record();
		jsp.set("count", daojuList.getTotalRow());
		jsp.set("data", daojuList.getList());
		jsp.set("msg", "获取成功");
		renderJson(jsp);
	}
	/**IFS单独处理入库
	 * @author simple
	 * @contact 15228717200
	 * @time 2022年5月9日 上午11:06:46
	 */
	public void frockIFSInstore() {
		Db.use("dc").tx(() -> {
			try {
				long materialId=getParaToLong("id");
				KMaterialList material=KMaterialList.dao.findById(materialId);
				material
				.setRemark("IFS手动入库")
				.setInstoreTime(new Date())
				.setStatus(2);//2-已入库，待工装管理员移库
				material.update();
				renderJson(Ret.ok("msg", "操作成功！"));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**IFS批量入库操作
	 * @author simple
	 * @contact 15228717200
	 * @time 2022年5月9日 上午11:21:04
	 */
	public void MulFrockIFSInstore() {
		Db.use("dc").tx(() -> {
			try {
				JSONArray selInstoreList=JSONArray.parseArray(get("selInstoreList"));
				for(int i = 0; i < selInstoreList.size(); i++) {
					JSONObject materialInfo=selInstoreList.getJSONObject(i);
					long materialId=materialInfo.getLongValue("id");
					KMaterialList material=KMaterialList.dao.findById(materialId);
					material
					.setRemark("IFS手动入库")
					.setInstoreTime(new Date())
					.setStatus(2);//2-已入库，待工装管理员移库
					material.update();
				}
				renderJson(Ret.ok("msg", "操作成功！"));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**
	 * 工装入库
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月8日 下午4:46:04
	 */
	@SuppressWarnings("rawtypes")
	public void frockInstore() {
		Record resp=new Record();
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long materialId=Long.valueOf(map.get("material_id").toString());
				KMaterialList material=KMaterialList.dao.findById(materialId);
				//判断ERP是否有数据，获取erp订单信息
				String sqlErp="select purchase.*,Purchase_Requisition_API.Get_Mark_For(purchase.REQUISITION_NO) as sqrxm,"
						+ "PERSON_INFO_API.Get_Name(Purchase_Requisition_Api.Get_Requisitioner_Code(purchase.REQUISITION_NO)) as sqjsr "
						+ "from PURCHASE_ORDER_LINE_NEW purchase where order_no = '"+material.getOrderNo()+"' and "
						+ "part_no='"+material.getPartNo()+"' and "
						+ "line_no='"+material.getOrderLineNo()+"' ";
				Record purchase=Db.use("oa").findFirst(sqlErp);
				if (purchase==null) {
					resp.set("code", 500);
					resp.set("msg", "ERP中未查询到订单号及零件号对应的采购订单！");
					return false;
				}else {
					String yu=purchase.getStr("contract");
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					String applicant_user=purchase.getStr("sqrxm");
					String application_receive=purchase.getStr("sqjsr");
					KDelivery delivery=KDelivery.dao.findById(material.getDeliveryId());
					//ERP订单入库
					Db.use("oa").execute(new ICallback() {
						@Override
						public Object call(Connection conn) throws SQLException {
						    CallableStatement procDO = null;
						    String locationWdr=map.get("location").toString();
						    //2022-9-8 wrd特殊字符及非空处理
						    if ("".equals(locationWdr)||"null".equals(locationWdr)||locationWdr==null) {
						    	locationWdr="*";
							}else {
								locationWdr = locationWdr.replace(".", "*").replace("%", "*").replace(">", "*")
										.replace("<", "*").replace("&", "*").replace("_", "*")
										.replace("^", "*").replace("~", "*").replace("/", "*")
										.replace(":", "*").replace("\\", "*");
							}
						    locationWdr=locationWdr.trim();
						    String wdr="";
						    if (locationWdr.length()>=15) {
						    	wdr=locationWdr.substring(0,15);
							}else {
								wdr=locationWdr;
							}
						    String searchNo="gz"+delivery.getDeliveryNo();
						    try {
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.Receive_Purchase_Order_API.Packed_Arrival__(?,?,?,?,?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, "");
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, "");
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        procDO.setString(4, null);
						        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
						        procDO.setString(5, null);
						        procDO.setString(6, "!\n$ENG_CHG_LEVEL=1"
						                       + "\n$ORDER_NO="+material.getOrderNo() 
						                       + "\n$LINE_NO="+material.getOrderLineNo()
						                       + "\n$RELEASE_NO="+purchase.getStr("release_no")
						                       + "\n$RECEIPT_REFERENCE="+searchNo//接收查询号
						                       + "\n$RECEIVER=IFSAPP"
						                       + "\n$QTY_ARRIVED="+material.getQty().intValue()
						                       + "\n$INV_QTY_ARRIVED="+material.getQty().intValue()
						                       + "\n$CATCH_QTY_ARRIVED="
						                       + "\n$QTY_TO_INSPECT=0"
						                       + "\n$ARRIVAL_DATE="+sdf.format(new Date())
						                       + "\n$RECEIVE_CASE=直接接收入库"
						                       + "\n$INPUT_VARIABLE_VALUES="
						                       + "\n$NOTE_TEXT="
						                       + "\n$QC_CODE=*"
						                       + "\n$CONTRACT="+yu
						                       + "\n$ACTIVITY_SEQ=0"
						                       + "\n$PART_NO="+material.getPartNo()
						                       + "\n$EXPIRATION_DATE="
						                       + "\n$INVENTORY_PART=TRUE"
						                       + "\n$LOCATION_NO="+"05"
						                       + "\n$LOT_BATCH_NO="+purchase.getStr("lot_batch_no")
						                       + "\n$SERIAL_NO=*"
						                       + "\n$CONDITION_CODE="
						                       + "\n$WAIV_DEV_REJ_NO="+wdr);
						        procDO.setString(7, "FALSE");
						        procDO.setString(8, "FALSE");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "入库成功！");
								return true;
						    } catch (SQLException e) {
						    	KErpRecords erpRecord0=new KErpRecords();
						    	erpRecord0.setReason(e.getMessage())
					    		.setCreateTime(new Date())
					    		.setInfo(material.toString()+"erp入库-中文+wdr："+wdr);
						    	erpRecord0.save();
					    		e.printStackTrace();
					    		System.err.println("入库失败中文"+new Date());
						    	try {
						    		procDO.setString(6, "!\n$ENG_CHG_LEVEL=1"
					                         + "\n$ORDER_NO="+material.getOrderNo()
					                         + "\n$LINE_NO="+material.getOrderLineNo()
					                         + "\n$RELEASE_NO="+purchase.getStr("release_no")
					                         + "\n$RECEIPT_REFERENCE="+searchNo
					                         + "\n$RECEIVER=IFSAPP"
					                         + "\n$QTY_ARRIVED="+material.getQty().intValue()
					                         + "\n$INV_QTY_ARRIVED="+material.getQty().intValue()
					                         + "\n$CATCH_QTY_ARRIVED="
					                         + "\n$QTY_TO_INSPECT=0"
					                         + "\n$ARRIVAL_DATE="+sdf.format(new Date())
					                         + "\n$RECEIVE_CASE=Receive into Inventory"
					                         + "\n$INPUT_VARIABLE_VALUES="
					                         + "\n$NOTE_TEXT="
					                         + "\n$QC_CODE=*"
					                         + "\n$CONTRACT="+yu
					                         + "\n$ACTIVITY_SEQ=0"
					                         + "\n$PART_NO="+material.getPartNo()
					                         + "\n$EXPIRATION_DATE="
					                         + "\n$INVENTORY_PART=TRUE"
					                         + "\n$LOCATION_NO="+"05"
					                         + "\n$LOT_BATCH_NO="+purchase.getStr("lot_batch_no")
					                         + "\n$SERIAL_NO=*"
					                         + "\n$CONDITION_CODE="
					                         + "\n$WAIV_DEV_REJ_NO="+wdr);
						    		procDO.execute();
						    		resp.set("code", 0);
									resp.set("msg", "收货成功！");
									return true;
						    	} catch (Exception e2) {
						    		KErpRecords erpRecord=new KErpRecords();
						    		erpRecord.setReason(e2.getMessage())
						    		.setCreateTime(new Date())
						    		.setInfo(material.toString()+"erp入库英文+wdr："+wdr);
						    		erpRecord.save();
						    		e2.printStackTrace();
						    		resp.set("code", 500);
						    		resp.set("msg", "入库失败英文！"+e2.getMessage());
						    		return false;
						    	}
						    }
						}
					});
					int code=resp.getInt("code");
					if (code==0) {
						material
						.setYu(yu)
						.setLocation(map.get("location").toString())
						.setRemark(map.get("remark").toString())
						.setInstoreTime(new Date())
						.setErpReceiveUser(application_receive)
						.setErpApplicantUser(applicant_user)
						.setStatus(2);//2-已入库，待工装管理员移库
						material.update();
						return true;
					}else {
						return false;
					}
				}
			});
			if (flag) {
				renderJson(Ret.ok("msg", "入库成功！"));
			}else {
				renderJson(Ret.fail("msg", resp.getStr("msg")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**批量工装入库
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年10月25日 上午11:38:57
	 */
	@SuppressWarnings("rawtypes")
	public void batchFrockInstore() {
		Db.tx(()->{
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				JSONArray selInstoreList=JSONArray.parseArray(get("selInstoreList"));
				boolean isAllHave=true;
				String msg="";
				for (int i = 0; i < selInstoreList.size(); i++) {
					JSONObject materialInfo=selInstoreList.getJSONObject(i);
					long materialId=materialInfo.getLongValue("id");
					KMaterialList material=KMaterialList.dao.findById(materialId);
					//判断ERP是否有数据，获取erp订单信息
					String sqlErp="select purchase.*,Purchase_Requisition_API.Get_Mark_For(purchase.REQUISITION_NO) as sqrxm,"
							+ "PERSON_INFO_API.Get_Name(Purchase_Requisition_Api.Get_Requisitioner_Code(purchase.REQUISITION_NO)) as sqjsr "
							+ "from PURCHASE_ORDER_LINE_NEW purchase where order_no = '"+material.getOrderNo()+"' and "
							+ "part_no='"+material.getPartNo()+"' and "
							+ "line_no='"+material.getOrderLineNo()+"' ";
					Record purchase=Db.use("oa").findFirst(sqlErp);
					if (purchase==null) {
						isAllHave=false;
						msg=material.getPartNo()+" 在ERP中未查询到订单号及零件号对应的采购订单！";
						break;
					}
				}
				boolean ifsSuccess=true;
				if (isAllHave) {
					for(int i = 0; i < selInstoreList.size(); i++) {
						//查询ERP中订单信息
						JSONObject materialInfo=selInstoreList.getJSONObject(i);
						long materialId=materialInfo.getLongValue("id");
						KMaterialList material=KMaterialList.dao.findById(materialId);
						//判断ERP是否有数据，获取erp订单信息
						String sqlErp="select purchase.*,Purchase_Requisition_API.Get_Mark_For(purchase.REQUISITION_NO) as sqrxm,"
								+ "PERSON_INFO_API.Get_Name(Purchase_Requisition_Api.Get_Requisitioner_Code(purchase.REQUISITION_NO)) as sqjsr "
								+ "from PURCHASE_ORDER_LINE_NEW purchase where order_no = '"+material.getOrderNo()+"' and "
								+ "part_no='"+material.getPartNo()+"' and "
								+ "line_no='"+material.getOrderLineNo()+"' ";
						Record purchase=Db.use("oa").findFirst(sqlErp);
						String yu=purchase.getStr("contract");
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
						String applicant_user=purchase.getStr("sqrxm");
						String application_receive=purchase.getStr("sqjsr");
						KDelivery delivery=KDelivery.dao.findById(material.getDeliveryId());
						//ERP订单入库
						Record resp=new Record();
						Db.use("oa").execute(new ICallback() {
							@Override
							public Object call(Connection conn) throws SQLException {
							    CallableStatement procDO = null;
							    String locationWdr=map.get("location").toString();
							    //2022-9-8 wrd特殊字符及非空处理
							    if ("".equals(locationWdr)||"null".equals(locationWdr)||locationWdr==null) {
							    	locationWdr="*";
								}else {
									locationWdr = locationWdr.replace(".", "*").replace("%", "*").replace(">", "*")
											.replace("<", "*").replace("&", "*").replace("_", "*")
											.replace("^", "*").replace("~", "*").replace("/", "*")
											.replace(":", "*").replace("\\", "*");
								}
							    locationWdr=locationWdr.trim();
							    String wdr="";
							    if (locationWdr.length()>=15) {
							    	wdr=locationWdr.substring(0,15);
								}else {
									wdr=locationWdr;
								}
							    String searchNo="gz"+delivery.getDeliveryNo();
							    try {
							    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.Receive_Purchase_Order_API.Packed_Arrival__(?,?,?,?,?,?,?,?)}");
							    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
							        procDO.setString(1, null);
							        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
							        procDO.setString(2, "");
							        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
							        procDO.setString(3, "");
							        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
							        procDO.setString(4, null);
							        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
							        procDO.setString(5, null);
							        procDO.setString(6, "!\n$ENG_CHG_LEVEL=1"
							                       + "\n$ORDER_NO="+material.getOrderNo() 
							                       + "\n$LINE_NO="+material.getOrderLineNo()
							                       + "\n$RELEASE_NO="+purchase.getStr("release_no")
							                       + "\n$RECEIPT_REFERENCE="+searchNo//接收查询号
							                       + "\n$RECEIVER=IFSAPP"
							                       + "\n$QTY_ARRIVED="+material.getQty().intValue()
							                       + "\n$INV_QTY_ARRIVED="+material.getQty().intValue()
							                       + "\n$CATCH_QTY_ARRIVED="
							                       + "\n$QTY_TO_INSPECT=0"
							                       + "\n$ARRIVAL_DATE="+sdf.format(new Date())
							                       + "\n$RECEIVE_CASE=直接接收入库"
							                       + "\n$INPUT_VARIABLE_VALUES="
							                       + "\n$NOTE_TEXT="
							                       + "\n$QC_CODE=*"
							                       + "\n$CONTRACT="+yu
							                       + "\n$ACTIVITY_SEQ=0"
							                       + "\n$PART_NO="+material.getPartNo()
							                       + "\n$EXPIRATION_DATE="
							                       + "\n$INVENTORY_PART=TRUE"
							                       + "\n$LOCATION_NO="+"05"
							                       + "\n$LOT_BATCH_NO="+purchase.getStr("lot_batch_no")
							                       + "\n$SERIAL_NO=*"
							                       + "\n$CONDITION_CODE="
							                       + "\n$WAIV_DEV_REJ_NO="+wdr);
							        procDO.setString(7, "FALSE");
							        procDO.setString(8, "FALSE");
							        procDO.execute();
							        resp.set("code", 0);
									resp.set("msg", "入库成功！");
									return true;
							    } catch (SQLException e) {
							    	KErpRecords erpRecord0=new KErpRecords();
							    	erpRecord0.setReason(e.getMessage())
						    		.setCreateTime(new Date())
						    		.setInfo(material.toString()+"erp入库-中文+wdr："+wdr);
							    	erpRecord0.save();
						    		e.printStackTrace();
						    		System.err.println("入库失败中文"+new Date());
							    	try {
							    		procDO.setString(6, "!\n$ENG_CHG_LEVEL=1"
						                         + "\n$ORDER_NO="+material.getOrderNo()
						                         + "\n$LINE_NO="+material.getOrderLineNo()
						                         + "\n$RELEASE_NO="+purchase.getStr("release_no")
						                         + "\n$RECEIPT_REFERENCE="+searchNo
						                         + "\n$RECEIVER=IFSAPP"
						                         + "\n$QTY_ARRIVED="+material.getQty().intValue()
						                         + "\n$INV_QTY_ARRIVED="+material.getQty().intValue()
						                         + "\n$CATCH_QTY_ARRIVED="
						                         + "\n$QTY_TO_INSPECT=0"
						                         + "\n$ARRIVAL_DATE="+sdf.format(new Date())
						                         + "\n$RECEIVE_CASE=Receive into Inventory"
						                         + "\n$INPUT_VARIABLE_VALUES="
						                         + "\n$NOTE_TEXT="
						                         + "\n$QC_CODE=*"
						                         + "\n$CONTRACT="+yu
						                         + "\n$ACTIVITY_SEQ=0"
						                         + "\n$PART_NO="+material.getPartNo()
						                         + "\n$EXPIRATION_DATE="
						                         + "\n$INVENTORY_PART=TRUE"
						                         + "\n$LOCATION_NO="+"05"
						                         + "\n$LOT_BATCH_NO="+purchase.getStr("lot_batch_no")
						                         + "\n$SERIAL_NO=*"
						                         + "\n$CONDITION_CODE="
						                         + "\n$WAIV_DEV_REJ_NO="+wdr);
							    		procDO.execute();
							    		resp.set("code", 0);
										resp.set("msg", "入库成功！");
										return true;
							    	} catch (Exception e2) {
							    		KErpRecords erpRecord=new KErpRecords();
							    		erpRecord.setReason(e2.getMessage())
							    		.setCreateTime(new Date())
							    		.setInfo(material.toString()+"erp移库+wdr："+wdr);
							    		erpRecord.save();
							    		e2.printStackTrace();
							    		resp.set("code", 500);
							    		resp.set("msg", "移库失败！"+e2.getMessage());
							    		return false;
							    	}
							    }
							}
						});
						int code=resp.getInt("code");
						if (code==0) {
							material
							.setYu(yu)
							.setLocation(map.get("location").toString())
							.setRemark(map.get("remark").toString())
							.setInstoreTime(new Date())
							.setErpReceiveUser(application_receive)
							.setErpApplicantUser(applicant_user)
							.setStatus(2);//2-已入库，待工装管理员移库
							material.update();
						}else {
							ifsSuccess=false;
							msg=resp.getStr("msg");
							break;
						}
					}
				}else {
					ifsSuccess=false;
				}
				if (ifsSuccess) {
					renderJson(Ret.ok("msg", "入库成功！"));
					return true;
				}else {
					renderJson(Ret.fail("msg", msg));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
				return false;
			}
		});
	}
}
