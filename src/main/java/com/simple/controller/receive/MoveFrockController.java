package com.simple.controller.receive;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.FrockEditRecord;
import com.simple.common.model.Frocks;
import com.simple.common.model.KMaterialList;
import com.simple.common.model.UserRole;

/**
 * 工装移库管理 frockMove/towaitMoveList
 *
 * @author FL00024996
 */
public class MoveFrockController extends Controller {
    public void towaitMoveList() {
        render("frockMoveList.html");
    }

    public void getWaitMoveList() {
        OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
        List<UserRole> userRoles = UserRole.dao.find("select * from user_role where user_id='" + user.getUserid() + "'");
        int roleType = 0;
        for (int i = 0; i < userRoles.size(); i++) {
            if (userRoles.get(i).getRoleId() == 2) {
                roleType = 2;//超管
                break;
            } else if (userRoles.get(i).getRoleId() == 1) {
                roleType = 1;//工装管理员
                break;
            } else if (userRoles.get(i).getRoleId() == 5) {
                roleType = 5;//库房操作员
                break;
            }
        }
        String deliveryNo = get("deliveryNo");
        String supplierName = get("supplierName");
        String orderNo = get("orderNo");
        String partNo = get("partNo");
        String partName = get("partName");
        int selStatus = getParaToInt("selStatus");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append(" from k_delivery a,k_material_list b where a.id=b.delivery_id and (b.status=2 or b.status=3) ");
        if (roleType == 1) {
            //查询同车间工装员
            sb.append(" and b.receive_user_id in (select user_id from user_ware where ware_id in (select ware_id from user_ware where user_id='" + user.getUserid() + "' and status=0) and status=0) ");
        } else if (roleType == 5) {//库房人员
            //		sb.append(" and b.frock_type=0 ");//技术中心
            sb.append(" ");
        } else if (roleType == 0) {
            sb.append(" and b.receive_user_id='" + user.getUserid() + "'");
        }
        if (!"".equals(deliveryNo) && deliveryNo != null) {
            sb.append(" and a.delivery_no like '%" + deliveryNo + "%' ");
        }
        if (!"".equals(supplierName) && supplierName != null) {
            sb.append(" and a.supplier_name like '%" + supplierName + "%' ");
        }
        if (!"".equals(orderNo) && orderNo != null) {
            sb.append(" and b.order_no like '%" + orderNo + "%' ");
        }
        if (!"".equals(partNo) && partNo != null) {
            sb.append(" and b.part_no like '%" + partNo + "%' ");
        }
        if (!"".equals(partName) && partName != null) {
            sb.append(" and b.part_desc like '%" + partName + "%' ");
        }
        if (selStatus != 99) {
            sb.append(" and b.status=" + selStatus);
        }
        Page<Record> daojuList = Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.delivery_no,a.supplier_name,a.deliver_lbn ", sb.toString() + " order by id desc");
        Record jsp = new Record();
        jsp.set("count", daojuList.getTotalRow());
        jsp.set("data", daojuList.getList());
        jsp.set("msg", "获取成功");
        renderJson(jsp);
    }

    /**
     * 移库
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月10日 上午9:57:31
     */
    @SuppressWarnings("rawtypes")
    public void moveFrock() {
        Record record = new Record();
        try {
            boolean flag = Db.tx(() -> {
                OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Map map = FastJson.getJson().parse(get("formData"), Map.class);

                Map moveFrockInfo = FastJson.getJson().parse(get("moveFrockInfo"), Map.class);
                String wareId = map.get("wareHouseId").toString();
                String barcode = map.get("barcode").toString().trim();
                //寿命
                double life = Double.parseDouble(map.get("theory_life").toString());
                double dbLife = Math.round(life * 10000);
                long theoryLife = new Double(dbLife).longValue();
                //判断编号是否重复
                String sqlFrock = "select * from frocks where barcode='" + barcode + "'";
                List<Record> haveFrockList = Db.use("dc").find(sqlFrock);
                if (haveFrockList.isEmpty()) {
                    //物料数量修改
                    KMaterialList material = KMaterialList.dao.findById(Long.valueOf(moveFrockInfo.get("id").toString()));
                    int haveMoveNum = material.getHaveMoveQty();
                    int moveQty = Integer.valueOf(map.get("total_num").toString());
                    if (haveMoveNum + moveQty == material.getQty()) {//已全部移库
                        material.setStatus(3).setHaveMoveQty(haveMoveNum + moveQty);
                        material.update();
                    } else {
                        material.setHaveMoveQty(haveMoveNum + moveQty);
                        material.update();
                    }
                    //工装台账新增数据
                    Frocks frock = getModel(Frocks.class);
                    String fileList = get("fileList");
                    JSONArray fileArray = JSONArray.parseArray(fileList);
                    JSONObject fileObject = fileArray.getJSONObject(0);
                    long fileId = fileObject.getLongValue("id");
                    Date nextCheckDate = null;
                    Date arriveDate = null;
                    try {
                        nextCheckDate = sdf.parse(map.get("next_check_date").toString());
                        arriveDate = sdf.parse(map.get("arrival_date").toString());

                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    frock.setMaterialId(material.getId())
                            .setWareId(Integer.valueOf(wareId))
                            .setBarcode(barcode)
                            .setIfsBarcode(map.get("ifs_barcode").toString())
                            .setFrockName(map.get("frock_name").toString())
                            .setFrockBarcode(map.get("frock_barcode").toString())
                            .setProduct(map.get("product").toString())
                            .setProduce(map.get("produce").toString())
                            .setProduceName(map.get("produce_name").toString())
                            .setTotalNum(moveQty).setInstoreNum(Integer.valueOf(map.get("total_num").toString()))
                            .setUnit(map.get("unit").toString())
                            .setTechUserId(map.get("tech_user_id").toString()).setTechUserName(map.get("tech_user_name").toString())
                            .setInformUserId(map.get("inform_user_id").toString()).setInformUserName(map.get("inform_user_name").toString())
                            .setArrivalDate(arriveDate)
                            .setLocation(map.get("location").toString())
                            .setKeepContent(map.get("keep_content").toString())
                            .setKeepMethod(map.get("keep_method").toString()).setNextCheckDate(nextCheckDate)
                            .setStatus(Integer.valueOf(map.get("status").toString()))
                            .setCreateUserId(user.getUserid()).setCreateUserName(user.getName())
                            .setAcceptId(fileId)
                            .setTheoryLife(theoryLife)
                            .setKeepDays(Integer.parseInt(map.get("keep_days").toString()));
                    frock.save();
                    //操作记录历史
                    FrockEditRecord editRecord = new FrockEditRecord();
                    editRecord.setFrockCode(barcode).setUserId(user.getUserid())
                            .setContent(map.toString()).setCreateTime(new Date());
                    editRecord.save();
                    record.set("code", 0).set("msg", "移库成功！");
                    return true;
                } else {
                    record.set("code", 500).set("msg", "工装编号已存在，请重新填写");
                    return false;
                }
            });
            if (flag) {
                renderJson(Ret.ok("msg", record.getStr("msg")));
            } else {
                renderJson(Ret.fail("msg", record.getStr("msg")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 易损件判定
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月10日 上午10:24:55
     */
    @SuppressWarnings("rawtypes")
    public void frockType() {
        try {
            boolean flag = Db.tx(() -> {
                Map map = FastJson.getJson().parse(get("formData"), Map.class);
                long materialId = Long.valueOf(map.get("material_id").toString());
                KMaterialList material = KMaterialList.dao.findById(materialId);
                int frocktype = Integer.valueOf(map.get("frock_type").toString());
                material.setFrockType(frocktype);
                material.update();
                return true;
            });
            if (flag) {
                renderJson(Ret.ok("msg", "确认成功！"));
            } else {
                renderJson(Ret.fail("msg", "确认失败，请联系管理员！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }
}
