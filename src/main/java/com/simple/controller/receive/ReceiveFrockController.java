package com.simple.controller.receive;

import java.util.Date;
import java.util.List;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KMaterialList;
import com.simple.common.model.UserRole;
/**
 * 收货管理 receiveFrock
 * @author FL00024996
 *
 */
public class ReceiveFrockController extends Controller {
	public void toReceiveList() {
		render("receiveList.html");
	}
	public void getReceiveList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		List<UserRole> userRoles=UserRole.dao.find("select * from user_role where user_id='"+user.getUserid()+"'");
		int roleType=0;
		for (int i = 0; i < userRoles.size(); i++) {
			if (userRoles.get(i).getRoleId()==2) {
				roleType=2;//超管
				break;
			}else if (userRoles.get(i).getRoleId()==1) {
				roleType=1;//工装管理员
				break;
			}else if (userRoles.get(i).getRoleId()==5) {
				roleType=5;//库房操作员
				break;
			}
		}
		String deliveryNo=get("deliveryNo");
		String supplierName=get("supplierName");
		String orderNo=get("orderNo");
		String partNo=get("partNo");
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from k_delivery a,k_material_list b where a.id=b.delivery_id and b.status=0 ");
		if (roleType==1) {
			//查询同车间工装员
			sb.append(" and b.receive_user_id in (select user_id from user_ware where ware_id in (select ware_id from user_ware where user_id='"+user.getUserid()+"' and status=0) and status=0) ");
		}else if (roleType==0) {
			sb.append(" and b.receive_user_id='"+user.getUserid()+"'");
		}
//		else if (roleType==5) {//库房人员
//			sb.append(" and b.frock_type=0");//技术中心
//		}
		if (!"".equals(deliveryNo)&&deliveryNo!=null) {
			sb.append(" and a.delivery_no like '%"+deliveryNo+"%' ");
		}
		if (!"".equals(supplierName)&&supplierName!=null) {
			sb.append(" and a.supplier_name like '%"+supplierName+"%' ");
		}
		if (!"".equals(orderNo)&&orderNo!=null) {
			sb.append(" and b.order_no like '%"+orderNo+"%' ");
		}
		if (!"".equals(partNo)&&partNo!=null) {
			sb.append(" and b.part_no like '%"+partNo+"%' ");
		}
		Page<Record> daojuList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.delivery_no,a.supplier_name,a.deliver_lbn ",sb.toString());
		Record jsp=new Record();
		jsp.set("count", daojuList.getTotalRow());
		jsp.set("data", daojuList.getList());
		jsp.set("msg", "获取成功");
		renderJson(jsp);
	}
	/**
	 * 确认合格
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月8日 上午9:19:20
	 */
	public void sureQualified() {
		try {
			boolean flag = Db.tx(() -> {
				long materialId=getParaToLong("id");
				KMaterialList material=KMaterialList.dao.findById(materialId);
				material.setSureDate(new Date())
				.setIsQualified(1)
				.setStatus(1);
				material.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "确认成功！"));
			}else {
				renderJson(Ret.fail("msg", "确认失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 不合格确认
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月8日 上午9:32:24
	 */
	public void notQualified() {
		try {
			boolean flag = Db.tx(() -> {
				long materialId=getParaToLong("id");
				KMaterialList material=KMaterialList.dao.findById(materialId);
				material.setSureDate(new Date()).setIsQualified(2)//2-不合格
				.setStatus(4);
				material.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "确认成功！"));
			}else {
				renderJson(Ret.fail("msg", "确认失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}

}
