package com.simple.controller.StockPart;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/**
 * ERP库存件 erpStockPart
 * @author FL00024996
 *
 */
public class ErpStockPartController extends Controller {
	public void toERPStockPartList() {
		render("erpStockPartList.html");
	}
	/**获取库存件数据
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 上午11:31:09
	 */
	public void getERPInstockPartList() {
		String selStockPartCode=get("selStockPartCode");
		String selProductCode=get("selProductCode");
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		String selStockPartName=get("selStockPartName");
		StringBuilder sb=new StringBuilder();
		sb.append(" from INVENTORY_PART where 1=1 ");
		if (selStockPartCode!=null&&!"".equals(selStockPartCode)) {
			sb.append(" and part_no like '%"+selStockPartCode+"%'");
		}
		if (selStockPartName!=null&&!"".equals(selStockPartName)) {
			sb.append(" and description like '%"+selStockPartName+"%'");
		}
		if (selProductCode!=null&&!"".equals(selProductCode)) {
			sb.append(" and part_product_code like '%"+selProductCode+"%'");
		}
		Page<Record> stockPartList=Db.use("oa").paginate(page, limit, "select * ",sb.toString());
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", stockPartList.getTotalRow());
		record.set("list", stockPartList.getList());
		renderJson(record);
	}
}
