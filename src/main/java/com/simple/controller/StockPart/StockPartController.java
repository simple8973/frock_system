package com.simple.controller.StockPart;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.InAccount;
import com.simple.common.model.InLocation;
import com.simple.common.model.InProduct;
import com.simple.common.model.InStockPart;
import com.simple.common.model.InUnits;
import com.simple.common.model.KCompanyField;
import com.simple.common.model.UserRole;

import oracle.jdbc.OracleTypes;
/**
 * 库存件 stockPart/toStockPartList
 * @author FL00024996
 *
 */
public class StockPartController extends Controller {
	public void toStockPartList() {
		render("stockPartList.html");
	}
	/**获取库存件数据（从前端获取）
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 上午11:31:09
	 */
	public void getInstockPartList() {
		String selStockPartCode=get("selStockPartCode");
		String selProductCode=get("selProductCode");
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		String selStockPartName=get("selStockPartName");
		StringBuilder sb=new StringBuilder();
		sb.append(" from in_stock_part where status=0 ");
		if (selStockPartCode!=null&&!"".equals(selStockPartCode)) {
			sb.append(" and stock_part_code like '%"+selStockPartCode+"%'");
		}
		if (selStockPartName!=null&&!"".equals(selStockPartName)) {
			sb.append(" and stock_part_name like '%"+selStockPartName+"%'");
		}
		if (selProductCode!=null&&!"".equals(selProductCode)) {
			sb.append(" and product_code like '%"+selProductCode+"%'");
		}
		sb.append("order by id desc");
		Page<Record> stockPartList=Db.use("dc").paginate(page, limit, "select * ",sb.toString());
		Record record=new Record();
		//角色（获取拥有权限为2角色的用户 user_role表中的记录）
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		UserRole role=UserRole.dao.findFirst("select * from user_role where user_id='"+user.getUserid()+"' and role_id=2");
		if (role==null) {
			record.set("role", 0);
		}else {
			record.set("role", 2);
		}
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", stockPartList.getTotalRow());
		record.set("list", stockPartList.getList());
		renderJson(record);
	}
	/**新增
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 下午2:56:31
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void saveAddStockPart() {
		Record resp=new Record();
		Db.tx(()->{
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			String stock_part_code=map.get("stock_part_code").toString();
			String stock_part_name=map.get("stock_part_name").toString();
			String field=map.get("field").toString();
			String part_type=map.get("part_type").toString();
			String plan_user=map.get("plan_user").toString();
			String account_code=map.get("account_code").toString();
			String unit=map.get("unit").toString();
			String product_code=map.get("product_code").toString();
				InUnits inUnit=InUnits.dao.findFirst("select * from in_units where unit='"+unit+"'");
			Db.use("oa").execute(new ICallback() {
				@Override
				public Object call(Connection conn) throws SQLException {
					//查询零件目录是否存在
					List<Record> havePartCatlog=Db.use("oa").find("select * from PART_CATALOG where part_no='"+stock_part_code+"'");
					if (havePartCatlog.size()==0) {
						try {
							//ERP创建零件目录---人工中文
							CallableStatement procDO_catalog = null;
							procDO_catalog = (CallableStatement) conn.prepareCall("{call ifsapp.PART_CATALOG_API.NEW__(?,?,?,?,?)}");
							procDO_catalog.registerOutParameter(1, OracleTypes.VARCHAR);
							procDO_catalog.setString(1, null);
							procDO_catalog.registerOutParameter(2, OracleTypes.VARCHAR);
							procDO_catalog.setString(2, null);
							procDO_catalog.registerOutParameter(3, OracleTypes.VARCHAR);
							procDO_catalog.setString(3, null);
					        StringBuilder sb=new StringBuilder();
					        sb.append("PART_NO\037"+stock_part_code+"\036");
					        sb.append("DESCRIPTION\037"+stock_part_name+"\036");
					        sb.append("UNIT_CODE\037"+unit+"\036");
					        sb.append("STD_NAME_ID\037"+"0"+"\036");
					        sb.append("FREIGHT_FACTOR\037"+"1"+"\036");
					        sb.append("POSITION_PART_DB\037"+"NOT POSITION PART"+"\036");
					        sb.append("CONDITION_CODE_USAGE_DB\037"+"NOT_ALLOW_COND_CODE"+"\036");
					        sb.append("CONFIGURABLE_DB\037"+"NOT CONFIGURED"+"\036");
					        sb.append("CATCH_UNIT_ENABLED_DB\037"+"FALSE"+"\036");
					        sb.append("MULTILEVEL_TRACKING_DB\037"+"TRACKING_OFF"+"\036");
					        sb.append("ALLOW_AS_NOT_CONSUMED_DB\037"+"FALSE"+"\036");
					        sb.append("RECEIPT_ISSUE_SERIAL_TRACK_DB\037"+"FALSE"+"\036");
					        sb.append("SERIAL_TRACKING_CODE_DB\037"+"NOT SERIAL TRACKING"+"\036");
					        sb.append("ENG_SERIAL_TRACKING_CODE_DB\037"+"NOT SERIAL TRACKING"+"\036");
					        sb.append("STOP_ARRIVAL_ISSUED_SERIAL_DB\037"+"TRUE"+"\036");
					        sb.append("STOP_NEW_SERIAL_IN_RMA_DB\037"+"TRUE"+"\036");
					        sb.append("SERIAL_RULE\037"+"人工"+"\036");
					        sb.append("LOT_TRACKING_CODE\037"+"无批跟踪"+"\036");
					        sb.append("LOT_QUANTITY_RULE\037"+"每个订单一个批量"+"\036");
					        sb.append("SUB_LOT_RULE\037"+"不允许子批"+"\036");
					        sb.append("COMPONENT_LOT_RULE\037"+"允许多个批次"+"\036");
					        procDO_catalog.setString(4, sb.toString());
					        procDO_catalog.setString(5, "DO");
					        procDO_catalog.execute();
					        try {
						    	//ERP新建库存件
						    	CallableStatement procDO = null;
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.NEW__(?,?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, null);
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, null);
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        StringBuilder sb_stock=new StringBuilder();
						        sb_stock.append("PART_NO\037"+stock_part_code+"\036");
						        sb_stock.append("DESCRIPTION\037"+stock_part_name+"\036");
						        sb_stock.append("CONTRACT\037"+field+"\036");
						        sb_stock.append("TYPE_CODE\037"+part_type+"\036");
						        sb_stock.append("PLANNER_BUYER\037"+plan_user+"\036");
						        sb_stock.append("UNIT_MEAS\037"+unit+"\036");
						        sb_stock.append("CATCH_UNIT_MEAS\037"+""+"\036");
						        sb_stock.append("ASSET_CLASS\037"+"S"+"\036");//资产等级
						        sb_stock.append("PART_STATUS\037"+"A"+"\036");//零件状态
						        sb_stock.append("ACCOUNTING_GROUP\037"+account_code+"\036");//会计组
						        sb_stock.append("PART_PRODUCT_CODE\037"+product_code+"\036");//产品代码
						        sb_stock.append("LEAD_TIME_CODE\037"+"采购"+"\036");//与零件类型关联
						        sb_stock.append("PURCH_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("MANUF_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("EXPECTED_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("MIN_DURAB_DAYS_CO_DELIV\037"+"0"+"\036");
						        sb_stock.append("MIN_DURAB_DAYS_PLANNING\037"+"0"+"\036");
						        sb_stock.append("SUPPLY_CODE\037"+"库存订单"+"\036");
						        sb_stock.append("DOP_CONNECTION\037"+"手动 DOP"+"\036");
						        sb_stock.append("DOP_NETTING\037"+"计算净值"+"\036");
						        sb_stock.append("QTY_CALC_ROUNDING\037"+inUnit.getRoundingUpNum().intValue()+"\036");
						        sb_stock.append("ZERO_COST_FLAG\037"+"禁止零成本"+"\036");
						        sb_stock.append("CYCLE_PERIOD\037"+"0"+"\036");
						        sb_stock.append("CYCLE_CODE_DB\037"+"N"+"\036");
						        sb_stock.append("STOCK_MANAGEMENT_DB\037"+"SYSTEM MANAGED INVENTORY"+"\036");
						        sb_stock.append("PALLET_HANDLED_DB\037"+"FALSE"+"\036");
						        sb_stock.append("DESCRIPTION_COPY\037"+stock_part_name+"\036\r\n");
						        procDO.setString(4, sb_stock.toString());
						        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
						        procDO.setString(5, "DO");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "ERP建号成功！");
								return true;
						    } catch (SQLException e4) {
						    	e4.printStackTrace();
					    		resp.set("code", 500);
					    		resp.set("msg", "ERP建号失败！"+e4.getMessage());
					    		return false;
						    }
						} catch (Exception e) {
							try {
								//ERP创建零件目录---人工英文
								CallableStatement procDO_catalog = null;
								procDO_catalog = (CallableStatement) conn.prepareCall("{call ifsapp.PART_CATALOG_API.NEW__(?,?,?,?,?)}");
								procDO_catalog.registerOutParameter(1, OracleTypes.VARCHAR);
								procDO_catalog.setString(1, null);
								procDO_catalog.registerOutParameter(2, OracleTypes.VARCHAR);
								procDO_catalog.setString(2, null);
								procDO_catalog.registerOutParameter(3, OracleTypes.VARCHAR);
								procDO_catalog.setString(3, null);
						        StringBuilder sb=new StringBuilder();
						        sb.append("PART_NO\037"+stock_part_code+"\036");
						        sb.append("DESCRIPTION\037"+stock_part_name+"\036");
						        sb.append("UNIT_CODE\037"+unit+"\036");
						        sb.append("STD_NAME_ID\037"+"0"+"\036");
						        sb.append("FREIGHT_FACTOR\037"+"1"+"\036");
						        sb.append("POSITION_PART_DB\037"+"NOT POSITION PART"+"\036");
						        sb.append("CONDITION_CODE_USAGE_DB\037"+"NOT_ALLOW_COND_CODE"+"\036");
						        sb.append("CONFIGURABLE_DB\037"+"NOT CONFIGURED"+"\036");
						        sb.append("CATCH_UNIT_ENABLED_DB\037"+"FALSE"+"\036");
						        sb.append("MULTILEVEL_TRACKING_DB\037"+"TRACKING_OFF"+"\036");
						        sb.append("ALLOW_AS_NOT_CONSUMED_DB\037"+"FALSE"+"\036");
						        sb.append("RECEIPT_ISSUE_SERIAL_TRACK_DB\037"+"FALSE"+"\036");
						        sb.append("SERIAL_TRACKING_CODE_DB\037"+"NOT SERIAL TRACKING"+"\036");
						        sb.append("ENG_SERIAL_TRACKING_CODE_DB\037"+"NOT SERIAL TRACKING"+"\036");
						        sb.append("STOP_ARRIVAL_ISSUED_SERIAL_DB\037"+"TRUE"+"\036");
						        sb.append("STOP_NEW_SERIAL_IN_RMA_DB\037"+"TRUE"+"\036");
						        sb.append("SERIAL_RULE\037"+"Manual"+"\036");
						        sb.append("LOT_TRACKING_CODE\037"+"Not Lot Tracking"+"\036");
						        sb.append("LOT_QUANTITY_RULE\037"+"One Lot Per Production Order"+"\036");
						        sb.append("SUB_LOT_RULE\037"+"No Sub Lots Allowed"+"\036");
						        sb.append("COMPONENT_LOT_RULE\037"+"Many Lots Allowed"+"\036");
						        procDO_catalog.setString(4, sb.toString());
						        procDO_catalog.setString(5, "DO");
						        procDO_catalog.execute();
						        try {
							    	//ERP新建库存件
							    	CallableStatement procDO = null;
							    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.NEW__(?,?,?,?,?)}");
							    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
							        procDO.setString(1, null);
							        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
							        procDO.setString(2, null);
							        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
							        procDO.setString(3, null);
							        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
							        StringBuilder sb_stock=new StringBuilder();
							        sb_stock.append("PART_NO\037"+stock_part_code+"\036");
							        sb_stock.append("DESCRIPTION\037"+stock_part_name+"\036");
							        sb_stock.append("CONTRACT\037"+field+"\036");
							        sb_stock.append("TYPE_CODE\037"+"Purchased (raw)"+"\036");
							        sb_stock.append("PLANNER_BUYER\037"+plan_user+"\036");
							        sb_stock.append("UNIT_MEAS\037"+unit+"\036");
							        sb_stock.append("CATCH_UNIT_MEAS\037"+""+"\036");
							        sb_stock.append("ASSET_CLASS\037"+"S"+"\036");//资产等级
							        sb_stock.append("PART_STATUS\037"+"A"+"\036");//零件状态
							        sb_stock.append("ACCOUNTING_GROUP\037"+account_code+"\036");//会计组
							        sb_stock.append("PART_PRODUCT_CODE\037"+product_code+"\036");//产品代码
							        sb_stock.append("LEAD_TIME_CODE\037"+"Purchased"+"\036");//与零件类型关联
							        sb_stock.append("PURCH_LEADTIME\037"+"0"+"\036");
							        sb_stock.append("MANUF_LEADTIME\037"+"0"+"\036");
							        sb_stock.append("EXPECTED_LEADTIME\037"+"0"+"\036");
							        sb_stock.append("MIN_DURAB_DAYS_CO_DELIV\037"+"0"+"\036");
							        sb_stock.append("MIN_DURAB_DAYS_PLANNING\037"+"0"+"\036");
							        sb_stock.append("SUPPLY_CODE\037"+"Inventory Order"+"\036");
							        sb_stock.append("DOP_CONNECTION\037"+"Manual DOP"+"\036");
							        sb_stock.append("DOP_NETTING\037"+"Netting"+"\036");
							        sb_stock.append("QTY_CALC_ROUNDING\037"+inUnit.getRoundingUpNum().intValue()+"\036");
							        sb_stock.append("ZERO_COST_FLAG\037"+"Zero Cost Forbidden"+"\036");
							        sb_stock.append("CYCLE_PERIOD\037"+"0"+"\036");
							        sb_stock.append("CYCLE_CODE_DB\037"+"N"+"\036");
							        sb_stock.append("STOCK_MANAGEMENT_DB\037"+"SYSTEM MANAGED INVENTORY"+"\036");
							        sb_stock.append("PALLET_HANDLED_DB\037"+"FALSE"+"\036");
							        sb_stock.append("DESCRIPTION_COPY\037"+stock_part_name+"\036\r\n");
							        procDO.setString(4, sb_stock.toString());
							        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
							        procDO.setString(5, "DO");
							        procDO.execute();
							        resp.set("code", 0);
									resp.set("msg", "ERP建号成功！");
									return true;
							    } catch (SQLException e3) {
							    	e3.printStackTrace();
						    		resp.set("code", 500);
						    		resp.set("msg", "ERP建号失败！"+e3.getMessage());
						    		return false;
							    }
							} catch (Exception e2) {
								e2.printStackTrace();
					    		resp.set("code", 500);
					    		resp.set("msg", "ERP创建零件目录失败！"+e2.getMessage());
					    		return false;
							}
						}
					}else {//跨域 有零件目录
						try {
					    	//ERP新建库存件
					    	CallableStatement procDO = null;
					    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.NEW__(?,?,?,?,?)}");
					    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
					        procDO.setString(1, null);
					        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
					        procDO.setString(2, null);
					        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
					        procDO.setString(3, null);
					        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
					        StringBuilder sb_stock=new StringBuilder();
					        sb_stock.append("PART_NO\037"+stock_part_code+"\036");
					        sb_stock.append("DESCRIPTION\037"+havePartCatlog.get(0).get("DESCRIPTION")+"\036"); 
					        sb_stock.append("CONTRACT\037"+field+"\036");
					        sb_stock.append("TYPE_CODE\037"+part_type+"\036");
					        sb_stock.append("PLANNER_BUYER\037"+plan_user+"\036");
					        sb_stock.append("UNIT_MEAS\037"+unit+"\036");
					        sb_stock.append("CATCH_UNIT_MEAS\037"+""+"\036");
					        sb_stock.append("ASSET_CLASS\037"+"S"+"\036");//资产等级
					        sb_stock.append("PART_STATUS\037"+"A"+"\036");//零件状态
					        sb_stock.append("ACCOUNTING_GROUP\037"+account_code+"\036");//会计组
					        sb_stock.append("PART_PRODUCT_CODE\037"+product_code+"\036");//产品代码
					        sb_stock.append("LEAD_TIME_CODE\037"+"采购"+"\036");//与零件类型关联
					        sb_stock.append("PURCH_LEADTIME\037"+"0"+"\036");
					        sb_stock.append("MANUF_LEADTIME\037"+"0"+"\036");
					        sb_stock.append("EXPECTED_LEADTIME\037"+"0"+"\036");
					        sb_stock.append("MIN_DURAB_DAYS_CO_DELIV\037"+"0"+"\036");
					        sb_stock.append("MIN_DURAB_DAYS_PLANNING\037"+"0"+"\036");
					        sb_stock.append("SUPPLY_CODE\037"+"库存订单"+"\036");
					        sb_stock.append("DOP_CONNECTION\037"+"手动 DOP"+"\036");
					        sb_stock.append("DOP_NETTING\037"+"计算净值"+"\036");
					        sb_stock.append("QTY_CALC_ROUNDING\037"+inUnit.getRoundingUpNum().intValue()+"\036");
					        sb_stock.append("ZERO_COST_FLAG\037"+"禁止零成本"+"\036");
					        sb_stock.append("CYCLE_PERIOD\037"+"0"+"\036");
					        sb_stock.append("CYCLE_CODE_DB\037"+"N"+"\036");
					        sb_stock.append("STOCK_MANAGEMENT_DB\037"+"SYSTEM MANAGED INVENTORY"+"\036");
					        sb_stock.append("PALLET_HANDLED_DB\037"+"FALSE"+"\036");
					        sb_stock.append("DESCRIPTION_COPY\037"+stock_part_name+"\036\r\n");
					        procDO.setString(4, sb_stock.toString());
					        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
					        procDO.setString(5, "DO");
					        procDO.execute();
					        resp.set("code", 0);
							resp.set("msg", "ERP建号成功！");
							return true;
					    } catch (Exception e4) {
					    	try {
					    		//ERP新建库存件
						    	CallableStatement procDO = null;
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.NEW__(?,?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, null);
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, null);
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        StringBuilder sb_stock=new StringBuilder();
						        sb_stock.append("PART_NO\037"+stock_part_code+"\036");
						        sb_stock.append("DESCRIPTION\037"+havePartCatlog.get(0).get("DESCRIPTION")+"\036"); 
						        sb_stock.append("CONTRACT\037"+field+"\036");
						        sb_stock.append("TYPE_CODE\037"+"Purchased (raw)"+"\036");
						        sb_stock.append("PLANNER_BUYER\037"+plan_user+"\036");
						        sb_stock.append("UNIT_MEAS\037"+unit+"\036");
						        sb_stock.append("CATCH_UNIT_MEAS\037"+""+"\036");
						        sb_stock.append("ASSET_CLASS\037"+"S"+"\036");//资产等级
						        sb_stock.append("PART_STATUS\037"+"A"+"\036");//零件状态
						        sb_stock.append("ACCOUNTING_GROUP\037"+account_code+"\036");//会计组
						        sb_stock.append("PART_PRODUCT_CODE\037"+product_code+"\036");//产品代码
						        sb_stock.append("LEAD_TIME_CODE\037"+"Purchased"+"\036");//与零件类型关联
						        sb_stock.append("PURCH_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("MANUF_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("EXPECTED_LEADTIME\037"+"0"+"\036");
						        sb_stock.append("MIN_DURAB_DAYS_CO_DELIV\037"+"0"+"\036");
						        sb_stock.append("MIN_DURAB_DAYS_PLANNING\037"+"0"+"\036");
						        sb_stock.append("SUPPLY_CODE\037"+"Inventory Order"+"\036");
						        sb_stock.append("DOP_CONNECTION\037"+"Manual DOP"+"\036");
						        sb_stock.append("DOP_NETTING\037"+"Netting"+"\036");
						        sb_stock.append("QTY_CALC_ROUNDING\037"+inUnit.getRoundingUpNum().intValue()+"\036");
						        sb_stock.append("ZERO_COST_FLAG\037"+"Zero Cost Forbidden"+"\036");
						        sb_stock.append("CYCLE_PERIOD\037"+"0"+"\036");
						        sb_stock.append("CYCLE_CODE_DB\037"+"N"+"\036");
						        sb_stock.append("STOCK_MANAGEMENT_DB\037"+"SYSTEM MANAGED INVENTORY"+"\036");
						        sb_stock.append("PALLET_HANDLED_DB\037"+"FALSE"+"\036");
						        sb_stock.append("DESCRIPTION_COPY\037"+stock_part_name+"\036\r\n");
						        procDO.setString(4, sb_stock.toString());
						        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
						        procDO.setString(5, "DO");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "ERP建号成功！");
								return true;
							} catch (SQLException e3) {
								e3.printStackTrace();
					    		resp.set("code", 500);
					    		resp.set("msg", "ERP建号失败！"+e3.getMessage());
					    		return false;
							}
					    }
					}
				}
			});
			if (resp.getInt("code")==0) {
				//本地数据库建号
				InStockPart stockPart=new InStockPart();
				stockPart._setAttrs(map);
				InAccount account=InAccount.dao.findFirst("select * from in_account where account_code='"+account_code+"'");
				stockPart.setAccountName(account.getAccountDesc());
				InProduct product=InProduct.dao.findFirst("select * from in_product where product_code='"+product_code+"'");
				OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
				stockPart.setProductName(product.getProductName())
				.setCreateTime(new Date())
				.setCreateUserId(user.getUserid()).setCreateUserName(user.getName())
				.setAssetLevelCode("S").setAssetLevelDesc("标准")
				.setPartStatusCode("A").setPartStatusDesc("ACTIVE");
				stockPart.save();
				renderJson(Ret.ok("msg","建号成功！"));
				return true;
			}else {
				renderJson(Ret.fail("msg",resp.get("msg")));
				return false;
			}
		});
	}
	/**编辑
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月10日 下午2:47:54
	 */
	@SuppressWarnings("rawtypes")
	public void saveEditStockPart() {
		Record resp=new Record();
		Db.tx(()->{
			long stockid=getParaToLong("stockid");
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			String stock_part_code=map.get("stock_part_code").toString();
			String stock_part_name=map.get("stock_part_name").toString();
			String account_code=map.get("account_code").toString();
			String product_code=map.get("product_code").toString();
			Db.use("oa").execute(new ICallback() {
				@Override
				public Object call(Connection conn) throws SQLException {
					//查询库存件
					Record erpStockPart=Db.use("oa").findFirst("select OBJVERSION,ROWIDTOCHAR(OBJID) objid from INVENTORY_PART where part_no='"+stock_part_code+"'");
					if (erpStockPart==null) {
			    		resp.set("code", 500);
			    		resp.set("msg", "ERP库存件不存在！");
			    		return false;
					}else {
						try {
					    	//ERP编辑库存件
					    	CallableStatement procDO = null;
					    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.MODIFY__(?,?,?,?,?)}");
					    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
					        procDO.setString(1, null);
					        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
					        procDO.setString(2, erpStockPart.getStr("OBJID"));
					        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
					        procDO.setString(3, erpStockPart.getStr("OBJVERSION"));
					        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
					        StringBuilder sb_stock=new StringBuilder();
					        sb_stock.append("DESCRIPTION\037"+stock_part_name+"\036");
					        sb_stock.append("ACCOUNTING_GROUP\037"+account_code+"\036");//会计组
					        sb_stock.append("PART_PRODUCT_CODE\037"+product_code+"\036");//产品代码
					        sb_stock.append("DESCRIPTION_COPY\037"+stock_part_name+"\036");
					        procDO.setString(4, sb_stock.toString());
					        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
					        procDO.setString(5, "DO");
					        procDO.execute();
					        resp.set("code", 0);
							resp.set("msg", "ERP修改成功！");
							return true;
					    } catch (SQLException e) {
					    	e.printStackTrace();
				    		resp.set("code", 500);
				    		resp.set("msg", "ERP修改失败！"+e.getMessage());
				    		return false;
					    }
					}
				}
			});
			if (resp.getInt("code")==0) {
				//本地数据库建号
				InStockPart stockPart=InStockPart.dao.findById(stockid);
				InAccount account=InAccount.dao.findFirst("select * from in_account where account_code='"+account_code+"'");
				stockPart.setAccountName(account.getAccountDesc());
				InProduct product=InProduct.dao.findFirst("select * from in_product where product_code='"+product_code+"'");
				stockPart.setProductCode(product_code).setAccountCode(account_code).setStockPartName(stock_part_name);
				stockPart.setProductName(product.getProductName());
				stockPart.update();
				renderJson(Ret.ok("msg","编辑成功！"));
				return true;
			}else {
				renderJson(Ret.fail("msg",resp.get("msg")));
				return false;
			}
		});
	}
	/**库位
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月10日 下午3:57:28
	 */
	@SuppressWarnings("rawtypes")
	public void saveEditStockLocation() {
		Record resp=new Record();
		Db.tx(()->{
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			String stock_part_code=map.get("stock_part_code").toString();
			String location_no=map.get("location_no").toString();
			long stockid=Long.valueOf(map.get("stock_id").toString());
			InStockPart stockPart=InStockPart.dao.findById(stockid);
			Db.use("oa").execute(new ICallback() {
				@Override
				public Object call(Connection conn) throws SQLException {
					//查询当前库存件是否存在库位信息---存在则删除
					Record haveLocation=Db.use("oa").findFirst("select OBJVERSION,ROWIDTOCHAR(OBJID) objid from INVENTORY_PART_DEF_LOC where part_no='"+stock_part_code+"'");
					if (haveLocation!=null) {
						try {
					    	//删除库位信息
					    	CallableStatement procDO_del = null;
					    	procDO_del = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_DEF_LOC_API.REMOVE__(?,?,?,?)}");
					    	procDO_del.registerOutParameter(1, OracleTypes.VARCHAR);
					    	procDO_del.setString(1, null);
					    	procDO_del.registerOutParameter(2, OracleTypes.VARCHAR);
					    	procDO_del.setString(2, haveLocation.getStr("objid"));
					    	procDO_del.registerOutParameter(3, OracleTypes.VARCHAR);
					    	procDO_del.setString(3, haveLocation.getStr("OBJVERSION"));
					    	procDO_del.registerOutParameter(4, OracleTypes.VARCHAR);
					    	procDO_del.setString(4, "DO");
					    	procDO_del.execute();
					        //新增
							try {
						    	//ERP编辑库存件
						    	CallableStatement procDO = null;
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_DEF_LOC_API.NEW__(?,?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, null);
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, null);
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        StringBuilder sb_stock=new StringBuilder();
						        sb_stock.append("CONTRACT\037"+stockPart.getField()+"\036");
						        sb_stock.append("PART_NO\037"+stock_part_code+"\036");//会计组
						        sb_stock.append("LOCATION_NO\037"+location_no+"\036");//产品代码
						        sb_stock.append("LOCATION_TYPE\037"+"提货"+"\036");
						        procDO.setString(4, sb_stock.toString());
						        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
						        procDO.setString(5, "DO");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "ERP库位新增成功！");
								return true;
						    } catch (SQLException e1) {
						    	try {
						    		//ERP编辑库存件---英文
							    	CallableStatement procDO = null;
							    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_DEF_LOC_API.NEW__(?,?,?,?,?)}");
							    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
							        procDO.setString(1, null);
							        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
							        procDO.setString(2, null);
							        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
							        procDO.setString(3, null);
							        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
							        StringBuilder sb_stock=new StringBuilder();
							        sb_stock.append("CONTRACT\037"+stockPart.getField()+"\036");
							        sb_stock.append("PART_NO\037"+stock_part_code+"\036");//会计组
							        sb_stock.append("LOCATION_NO\037"+location_no+"\036");//产品代码
							        sb_stock.append("LOCATION_TYPE\037"+"Picking"+"\036");
							        procDO.setString(4, sb_stock.toString());
							        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
							        procDO.setString(5, "DO");
							        procDO.execute();
							        resp.set("code", 0);
									resp.set("msg", "ERP库位新增成功！");
									return true;
								} catch (Exception e2) {
									e2.printStackTrace();
						    		resp.set("code", 500);
						    		resp.set("msg", "ERP库位新增失败！"+e2.getMessage());
						    		return false;
								}
						    }
					    } catch (SQLException e3) {
					    	e3.printStackTrace();
				    		resp.set("code", 500);
				    		resp.set("msg", "ERP库位删除失败！"+e3.getMessage());
				    		return false;
					    }
					}else {
						//新增
						try {
					    	//ERP编辑库存件
					    	CallableStatement procDO = null;
					    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_DEF_LOC_API.NEW__(?,?,?,?,?)}");
					    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
					        procDO.setString(1, null);
					        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
					        procDO.setString(2, null);
					        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
					        procDO.setString(3, null);
					        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
					        StringBuilder sb_stock=new StringBuilder();
					        sb_stock.append("CONTRACT\037"+stockPart.getField()+"\036");
					        sb_stock.append("PART_NO\037"+stock_part_code+"\036");//会计组
					        sb_stock.append("LOCATION_NO\037"+location_no+"\036");//产品代码
					        sb_stock.append("LOCATION_TYPE\037"+"提货"+"\036");
					        procDO.setString(4, sb_stock.toString());
					        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
					        procDO.setString(5, "DO");
					        procDO.execute();
					        resp.set("code", 0);
							resp.set("msg", "ERP库位新增成功！");
							return true;
					    } catch (SQLException e1) {
					    	try {
					    		//ERP编辑库存件---英文
						    	CallableStatement procDO = null;
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_DEF_LOC_API.NEW__(?,?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, null);
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, null);
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        StringBuilder sb_stock=new StringBuilder();
						        sb_stock.append("CONTRACT\037"+stockPart.getField()+"\036");
						        sb_stock.append("PART_NO\037"+stock_part_code+"\036");//会计组
						        sb_stock.append("LOCATION_NO\037"+location_no+"\036");//产品代码
						        sb_stock.append("LOCATION_TYPE\037"+"Picking"+"\036");
						        procDO.setString(4, sb_stock.toString());
						        procDO.registerOutParameter(5, OracleTypes.VARCHAR);
						        procDO.setString(5, "DO");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "ERP库位新增成功！");
								return true;
							} catch (Exception e2) {
								e2.printStackTrace();
					    		resp.set("code", 500);
					    		resp.set("msg", "ERP库位新增失败！"+e2.getMessage());
					    		return false;
							}
					    }
					}
				}
			});
			if (resp.getInt("code")==0) {
				stockPart.setLocationNo(location_no);
				stockPart.update();
				renderJson(Ret.ok("msg","编辑成功！"));
				return true;
			}else {
				renderJson(Ret.fail("msg",resp.get("msg")));
				return false;
			}
		});
	}
	/**删除
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月10日 下午5:42:57
	 */
	public void delStock() {
		Record resp=new Record();
		Db.tx(()->{
			long stockId=getParaToLong("id");
			InStockPart stockPart=InStockPart.dao.findById(stockId);
			String stock_part_code=stockPart.getStockPartCode();
			Db.use("oa").execute(new ICallback() {
				@Override
				public Object call(Connection conn) throws SQLException {
					//查询采购件、事务历史是否存在
					List<Record> historyList=Db.use("oa").find("select * from INVENTORY_TRANSACTION_HIST where part_no='"+stock_part_code+"'");
					List<Record> purchaseList=Db.use("oa").find("select * from PURCHASE_REQ_LINE_ALL where part_no='"+stock_part_code+"'");
					if (!historyList.isEmpty()||!purchaseList.isEmpty()) {
						resp.set("code", 500);
			    		resp.set("msg", "此零件已存在采购件行或事务历史！");
			    		return false;
					}else {
						Record erpStockPart=Db.use("oa").findFirst("select OBJVERSION,ROWIDTOCHAR(OBJID) objid from INVENTORY_PART where part_no='"+stock_part_code+"'");
						if (erpStockPart==null) {
				    		resp.set("code", 0);
				    		resp.set("msg", "ERP库存件不存在！");
				    		return false;
						}else {
							try {
						    	//ERP编辑库存件
						    	CallableStatement procDO = null;
						    	procDO = (CallableStatement) conn.prepareCall("{call ifsapp.INVENTORY_PART_API.REMOVE__(?,?,?,?)}");
						    	procDO.registerOutParameter(1, OracleTypes.VARCHAR);
						        procDO.setString(1, null);
						        procDO.registerOutParameter(2, OracleTypes.VARCHAR);
						        procDO.setString(2, erpStockPart.getStr("OBJID"));
						        procDO.registerOutParameter(3, OracleTypes.VARCHAR);
						        procDO.setString(3, erpStockPart.getStr("OBJVERSION"));
						        procDO.registerOutParameter(4, OracleTypes.VARCHAR);
						        procDO.setString(4, "DO");
						        procDO.execute();
						        resp.set("code", 0);
								resp.set("msg", "ERP删除成功！");
								return true;
						    } catch (SQLException e) {
						    	e.printStackTrace();
					    		resp.set("code", 500);
					    		resp.set("msg", "ERP删除失败！"+e.getMessage());
					    		return false;
						    }
						}
					}
				}
			});
			if (resp.getInt("code")==0) {
				stockPart.setStatus(9);
				stockPart.update();
				renderJson(Ret.ok("msg","删除成功！"));
				return true;
			}else {
				renderJson(Ret.fail("msg",resp.get("msg")));
				return false;
			}
		});
	}
	/**获取公司域列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 下午1:54:32
	 */
	public void getCompanyField() {
		List<KCompanyField> fields=KCompanyField.dao.find("select * from k_company_field");
		renderJson(Ret.ok("data",fields));
	}
	/**获取会计组
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 下午1:58:39
	 */
	public void getAccountList() {
		List<InAccount> accounts=InAccount.dao.find("select * from in_account");
		renderJson(Ret.ok("data",accounts));
	}
	/**获取计量单位
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 下午2:09:28
	 */
	public void getUnitList() {
		List<InUnits> units=InUnits.dao.find("select * from in_units");
		renderJson(Ret.ok("data",units));
	}
	/**获取产品列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 下午2:09:28
	 */
	public void getProductList() {
		List<InProduct> units=InProduct.dao.find("select * from in_product");
		renderJson(Ret.ok("data",units));
	}
	/**获取库位列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月10日 下午3:45:58
	 */
	public void getLocationList() {
		List<InLocation> locations=InLocation.dao.find("select * from in_location");
		renderJson(Ret.ok("data",locations));
	}
	/**初始化
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月11日 上午9:52:25
	 */
//	public void initStock() {
//		Db.tx(()->{
//			for (int j = 1; j < 10000; j++) {
//				Page<Record> erpStockList=Db.use("oa").paginate(j, 100, "select * "," FROM INVENTORY_PART where type_code_db=3");
//				for (int i = 0; i < erpStockList.getList().size(); i++) {
//					//查询库位
//					Record location=Db.use("oa").findFirst("select * from INVENTORY_PART_DEF_LOC where part_no='"+erpStockList.getList().get(i).getStr("part_no")+"'");
//					String location_no="";
//					if (location!=null) {
//						location_no=location.getStr("location_no");
//					}
//					//产品信息
//					String productName="";
//					InProduct product=InProduct.dao.findFirst("select * from in_product where product_code='"+erpStockList.getList().get(i).getStr("part_product_code")+"'");
//					if (product!=null) {
//						productName=product.getProductName();
//					}	
//					//会计组
//					String accountName="";
//					InAccount account=InAccount.dao.findFirst("select * from in_account where account_code='"+erpStockList.getList().get(i).getStr("accounting_group")+"'");
//					if (account!=null) {
//						accountName=account.getAccountDesc();
//					}
//					InStockPart stockPart=new InStockPart();
//					stockPart.setStockPartCode(erpStockList.getList().get(i).getStr("part_no")).setStockPartName(erpStockList.getList().get(i).getStr("description"))
//					.setField(erpStockList.getList().get(i).getStr("contract"))
//					.setPartType(erpStockList.getList().get(i).getStr("type_code"))
//					.setAccountCode(erpStockList.getList().get(i).getStr("accounting_group")).setAccountName(accountName)
//					.setPlanUser("*")
//					.setUnit(erpStockList.getList().get(i).getStr("unit_meas"))
//					.setProductCode(erpStockList.getList().get(i).getStr("part_product_code")).setProductName(productName)
//					.setCreateUserName("ERP旧数据")
//					.setAssetLevelCode("S").setAssetLevelDesc("标准")
//					.setPartStatusCode("A").setPartStatusDesc("ACTIVE")
//					.setStatus(0)
//					.setCreateTime(erpStockList.getList().get(i).get("create_date"))
//					.setLocationNo(location_no);
//					stockPart.save();
//				}
//				if (erpStockList.getList().size()!=100) {
//					break;
//				}
//			}
//			renderJson(Ret.ok("msg","初始化成功！"));
//			return true;
//		});
//	}
}
