package com.simple.controller.app;
/**
 * app领用
 */
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.common.model.UserRole;
import com.simple.ding.DingUtil;

public class AppLingYongController extends Controller {
	//处理领用
	public void doLingyong() {
		String userId=get("userId");
		Record jsp=new Record();
		if (userId==null||"".equals(userId)) {
			jsp.set("code", 200);
			jsp.set("msg", "员工ID有误，请确认！");
		}else {
			String userName=get("userName");
			long frockId=getParaToLong("frockId");
			int lingyongNum=getParaToInt("lingyongNum");
			String purpose=get("purpose");
			String manager_user_id=get("manager_user_id");
			String manager_user_name=get("manager_user_name");
			Date date=new Date();
			//领用
			ReceiveBack receive=getModel(ReceiveBack.class);
			receive.setFrockId(frockId)
			.setReceiveNum(lingyongNum)
			.setReceiveUserId(userId).setReceiveUserName(userName)
			.setPurpose(purpose)
			.setReceiveSpUserId(manager_user_id).setReceiveSpUserName(manager_user_name)
			.setReceiveDate(date)
			.setStatus(0);//0-领用待审批
			receive.save();
			//给工装管理员审批发送信息
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(manager_user_id, "工装领用通知：\n"+userName+" 发起工装领用审批，请登陆系统审批！"+sdf.format(date));
			//工装信息表中更新在库数量
			Frocks frock=Frocks.dao.findById(frockId);
			int instoreNum=frock.getInstoreNum();
			int newInStoreNum=instoreNum-lingyongNum;
			frock.setInstoreNum(newInStoreNum);
			frock.update();
			jsp.set("code", 0);
			jsp.set("msg", "等待审批！");
		}
		renderJson(jsp);
	}
	//编辑领用
	public void editLingyong() {
		long receiveId=getParaToLong("receiveId");
		String manager_user_id=get("manager_user_id");
		String manager_user_name=get("manager_user_name");
		
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		int lingyongNum=getParaToInt("lingyongNum");
		String purpose=get("purpose");
		Date date=new Date();
		receive.setReceiveNum(lingyongNum)
		.setPurpose(purpose)
		.setReceiveSpUserId(manager_user_id).setReceiveSpUserName(manager_user_name)
		.setReceiveDate(date)
		.setStatus(0);//0-领用待审批
		receive.update();
		//给工装管理员审批发送信息
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DingUtil.sendText(manager_user_id, "工装领用通知：\n"+receive.getReceiveUserName()+" 发起工装领用审批，请登陆系统审批！"+sdf.format(date));
		//工装信息表中更新在库数量
		Frocks frock=Frocks.dao.findById(receive.getFrockId());
		int instoreNum=frock.getInstoreNum();
		int newInStoreNum=instoreNum-lingyongNum;
		frock.setInstoreNum(newInStoreNum);
		frock.update();
		
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "等待审批！");
		renderJson(jsp);
	}
	//获取我的领用记录
	public void getMyReceive() {
		String user_id=get("user_id");
		String sqlReceive="select a.*,b.barcode,b.frock_name,b.frock_barcode "
				+ "from receive_back a,frocks b where a.receive_user_id='"+user_id+"' and b.id=a.frock_id and a.status<>9 order by a.status asc";
		List<Record> receives=Db.use("dc").find(sqlReceive);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Record jsp=new Record();
		if (receives.isEmpty()) {
			jsp.set("code", 1);
		}else {
			for (int i = 0; i < receives.size(); i++) {
				Date receive_date=receives.get(i).getDate("receive_date");
				receives.get(i).set("receivedate", sdf.format(receive_date));
			}
			jsp.set("code", 0);
		}
		jsp.set("data", receives);
		jsp.set("msg", "获取领取列表成功！");
		renderJson(jsp);
	}
	//删除我的领用
	public void delReceive() {
		long receiveId=getParaToLong("receiveId");
		ReceiveBack receiveBack=ReceiveBack.dao.findById(receiveId);
		receiveBack.setStatus(9);//以删除
		receiveBack.update();
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "删除成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	//根据工装id获取基本信息
	public void receiveInfo() {
		long receiveId=getParaToLong("receiveId");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Record jsp=new Record();
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		Frocks frock=Frocks.dao.findById(receive.getFrockId());
		String next_check_date=sdf.format(frock.getNextCheckDate());
		int status=frock.getStatus();
		if (status==0) {
			jsp.set("status_name", "可使用");
		}else if (status==1) {
			jsp.set("status_name", "封存");
		}else if (status==2) {
			jsp.set("status_name", "待修");
		}
		jsp.set("next_check_date", next_check_date);
		jsp.set("code", 0);
		jsp.set("msg", "获取成功！");
		jsp.set("receive", receive);
		jsp.set("frock", frock);
		renderJson(jsp);
	}
	//获取待审批列表
	public void getNewReceiveSp() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlReceive="select a.*,b.barcode,b.frock_name,b.frock_barcode "
				+ "from receive_back a,frocks b where a.status=0 and b.id=a.frock_id and a.receive_sp_user_id='"+user.getUserid()+"'";
		List<Record> receiveSpList=Db.use("dc").find(sqlReceive);
		Record jsp=new Record();
		if (receiveSpList.isEmpty()) {
			jsp.set("code", 1);
		}else {
			jsp.set("code", 0);
		}
		jsp.set("data", receiveSpList);
		jsp.set("msg", "获取待审批列表成功！");
		renderJson(jsp);
	}
	//审批通过
	public void spPassNewFrock() {
		long receiveId=getParaToLong("receiveId");
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		Record jsp=new Record();
		if (receive.getStatus()==0) {
			receive.setStatus(1);
			receive.update();
			Frocks frock=Frocks.dao.findById(receive.getFrockId());
			Date date=new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(receive.getReceiveUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】领用，已审批通过，请知悉！"+sdf.format(date));
			jsp.set("msg", "审批成功！");
		}else {
			jsp.set("msg", "请勿重复操作！");
		}
		jsp.set("code", 0);
		jsp.set("data", "");
		renderJson(jsp);
	}
	//审批驳回
	public void spBackNewFrock() {
		long receiveId=getParaToLong("receiveId");
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		Record jsp=new Record();
		jsp.set("code", 0);
		if (receive.getStatus()==0) {
			receive.setStatus(2);
			receive.update();
			//工装表在库数量增加
			Frocks frock=Frocks.dao.findById(receive.getFrockId());
			int instoreNum=frock.getInstoreNum();
			int receiveNum=receive.getReceiveNum();
			int newInstoreNum=instoreNum+receiveNum;
			frock.setInstoreNum(newInstoreNum);
			frock.update();
			Date date=new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(receive.getReceiveUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】领用，被审批驳回，请知悉！"+sdf.format(date));
			jsp.set("msg", "审批成功！");
		}else {
			jsp.set("msg", "请勿重复操作！");
		}
		jsp.set("data", "");
		renderJson(jsp);
	}
	//获取工装管理员
	public void getManagerList() {
		String sqlManager="select * from user_role where role_id=1";
		List<UserRole> userList=UserRole.dao.find(sqlManager);
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "获取工装管理员列表成功！");
		jsp.set("data", userList);
		renderJson(jsp);
	}
}
