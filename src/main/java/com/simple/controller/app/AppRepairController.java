package com.simple.controller.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.FrockRepair;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.common.model.UserRole;
import com.simple.ding.DingUtil;
/**
 * 工装报修appRepair
 * @author FL00024996
 *
 */
public class AppRepairController extends Controller {
	//获取班长列表
	public void getBZList() {
		String sqlBz="select * from user_role where role_id=3";
		List<UserRole> userList=UserRole.dao.find(sqlBz);
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "获取班长列表成功！");
		jsp.set("data", userList);
		renderJson(jsp);
	}
	//处理提交报修单
	public void doSqRepair() {
		String userId=get("userId");
		String userName=get("userName");
		long usedLife=getParaToLong("used_life");
		String repair_question=get("repair_question");
		long frockId=getParaToLong("frockId");
		String bz_user_id=get("bz_user_id");
		String bz_user_name=get("bz_user_name");
		String manager_user_id=get("manager_user_id");
		String manager_user_name=get("manager_user_name");
		
		Date date=new Date();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
		List<FrockRepair> lastRepairs=FrockRepair.dao.find("select * from frock_repair where sq_date='"+sdf1.format(date)+"'");
		int lastNum=1;
		if (!lastRepairs.isEmpty()) {
			lastNum=lastRepairs.size()+1;
		}
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		String repairNum=sdf.format(date)+lastNum;//单据号
		
		FrockRepair repair=getModel(FrockRepair.class);
		repair.setRepairNum(repairNum)
		.setFrockId(frockId)
		.setUsedLife(usedLife)
		.setSqUserId(userId).setSqUserName(userName)
		.setSqDate(date)
		.setProblem(repair_question)
		.setStatus(0)
		.setBzUserId(bz_user_id).setBzUserName(bz_user_name)
		.setManagerUserId(manager_user_id).setManagerUserName(manager_user_name);
		repair.save();
		
		SimpleDateFormat sdf0=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Frocks frock=Frocks.dao.findById(frockId);
		DingUtil.sendText(bz_user_id, "工装报修审批："+userName+"提交了\n工装编号【"+frock.getBarcode()+"】\n工装名称【"+frock.getFrockName()+"】\n的报修审批，请登录手机端进行操作！"+sdf0.format(date));
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "提交成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	//处理编辑报修
	public void editRepair() {
		long repairId=getParaToLong("repairId");
		String repair_question=get("repair_question");
		long usedLife=getParaToLong("used_life");
		String bz_user_id=get("bz_user_id");
		String bz_user_name=get("bz_user_name");
		String manager_user_id=get("manager_user_id");
		String manager_user_name=get("manager_user_name");
		
		Date date=new Date();
		SimpleDateFormat sdf0=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		FrockRepair repair=FrockRepair.dao.findById(repairId);
		repair.setUsedLife(usedLife)
		.setSqDate(date)
		.setProblem(repair_question)
		.setStatus(0)
		.setBzUserId(bz_user_id).setBzUserName(bz_user_name)
		.setManagerUserId(manager_user_id).setManagerUserName(manager_user_name);
		repair.update();
		
		Frocks frock=Frocks.dao.findById(repair.getFrockId());
		DingUtil.sendText(bz_user_id, "工装报修审批："+repair.getSqUserName()+"提交了\n工装编号【"+frock.getBarcode()+"】\n工装名称【"+frock.getFrockName()+"】\n的报修审批，请登录手机端进行操作！"+sdf0.format(date));
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "提交成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	//获取我的报修记录
	public void getMyRepair() {
		int status=getParaToInt("status");
		String user_id=get("user_id");
		String sqlRepair="select a.*,b.barcode,b.frock_name,b.frock_barcode from frock_repair a,frocks b "
				+ "where a.sq_user_id='"+user_id+"' and a.frock_id=b.id and a.status="+status
				+ " order by a.status asc";
		List<Record> repairList=Db.use("dc").find(sqlRepair);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Record jsp=new Record();
		if (repairList.isEmpty()) {
			jsp.set("code", 1);
		}else {
			for (int i = 0; i < repairList.size(); i++) {
				Date sq_date=repairList.get(i).getDate("sq_date");
				repairList.get(i).set("sqdate", sdf.format(sq_date));
			}
			jsp.set("code", 0);
		}
		jsp.set("data", repairList);
		jsp.set("repairsize", repairList.size());
		jsp.set("msg", "获取领取列表成功！");
		renderJson(jsp);
	}
	//删除报修记录
	public void delRepair() {
		long repairId=getParaToLong("repairId");
		FrockRepair repair=FrockRepair.dao.findById(repairId);
		repair.setStatus(9);
		repair.update();
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "删除成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	//根据报修id获取基本信息
	public void repairInfo() {
		long repairId=getParaToLong("repairId");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		FrockRepair repair=FrockRepair.dao.findById(repairId);
		Frocks frock=Frocks.dao.findById(repair.getFrockId());
		String next_check_date=sdf.format(frock.getNextCheckDate());
		String sqlReceive="select * from receive_back where frock_id="+frock.getId()+" and status=1";
		ReceiveBack receive=ReceiveBack.dao.findFirst(sqlReceive);
		if (repair.getReason()==null) {
			repair.setReason("");
		}
		if (repair.getResolution()==null) {
			repair.setResolution("");
		}
		Record jsp=new Record();
		jsp.set("next_check_date", next_check_date);
		jsp.set("code", 0);
		jsp.set("msg", "获取成功！");
		jsp.set("repair", repair);
		jsp.set("receive", receive);
		jsp.set("frock", frock);
		renderJson(jsp);
	}
	//获取待审批报修
	public void getSpRepair() {
		String userId=get("userId");
		int status=getParaToInt("status");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String sqlRepair="select a.*,b.barcode,b.frock_name,b.frock_barcode "
				+ "from frock_repair a,frocks b where a.status="+status+" and b.id=a.frock_id and a.bz_user_id='"+userId+"'";
		List<Record> repairSpList=Db.use("dc").find(sqlRepair);
		Record jsp=new Record();
		if (repairSpList.isEmpty()) {
			jsp.set("code", 1);
		}else {
			for (int i = 0; i < repairSpList.size(); i++) {
				Date sq_date=repairSpList.get(i).getDate("sq_date");
				repairSpList.get(i).set("sqdate", sdf.format(sq_date));
			}
			jsp.set("code", 0);
		}
		jsp.set("data", repairSpList);
		jsp.set("repairSize", repairSpList.size());
		jsp.set("msg", "获取待审批列表成功！");
		renderJson(jsp);
	}
	//审批通过
	public void spPassRepair() {
		long repairId=getParaToLong("repairId");
		Date date=new Date();
		FrockRepair repair=FrockRepair.dao.findById(repairId);
		repair.setStatus(1).setSpDate(date);
		repair.update();
		Frocks frock=Frocks.dao.findById(repair.getFrockId());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//通知申请人归还工装值管理员处
		DingUtil.sendText(repair.getSqUserId(), "工装报修通知：\n你提交的工装【"+frock.getFrockName()+"】报修，已审批通过。\n请及时将工装归还至管理员处！"+sdf.format(date));
		//通知管理员及时回收
		DingUtil.sendText(repair.getManagerUserId(), "工装报修通知："+repair.getSqUserName()+" 提交的\n工装编号【"+frock.getBarcode()+"】\n工装名称【"+frock.getFrockName()+"】\n报修已审批通过。\n请注意回收并登录电脑端操作！"+sdf.format(date));	
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "审批成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	//审批驳回
	public void spBackRepair() {
		Date date=new Date();
		long repairId=getParaToLong("repairId");
		FrockRepair repair=FrockRepair.dao.findById(repairId);
		repair.setStatus(2)
		.setSpDate(date);
		repair.update();
		Frocks frock=Frocks.dao.findById(repair.getFrockId());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//通知申请人归还工装值管理员处
		DingUtil.sendText(repair.getSqUserId(), "工装报修通知：\n你提交的工装【"+frock.getFrockName()+"】报修，已审批驳回。\n请确认！"+sdf.format(date));	
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "审批成功！");
		jsp.set("data", "");
		renderJson(jsp);
	}
	
}
