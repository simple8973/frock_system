package com.simple.controller.app;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.ding.DingUtil;
/**
 * 归还appBack
 */
public class AppBackController extends Controller {
	//扫码获取工装基本信息和领用表信息
	public void getBackFrockInfo() {
		String barcode=get("barcode");
		String sqlFrock="select * from frocks where barcode='"+barcode+"'";
		List<Frocks> frocks=Frocks.dao.find(sqlFrock);
		Record rd = new Record();
		if (frocks.isEmpty()) {
			rd.set("code", 1);
			rd.set("msg", "未查询到当前工装基本信息！");
		}else{
			//是否有领用记录
			Frocks frock=Frocks.dao.findFirst(sqlFrock);
			String sqlReceive="select * from receive_back where frock_id="+frock.getId()+" and status=1";
			List<ReceiveBack> receiveList=ReceiveBack.dao.find(sqlReceive);
			if (receiveList.isEmpty()) {
				rd.set("code", 1);
				rd.set("msg", "未查询到当前工装领用记录！");
			}else {
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				ReceiveBack receive=ReceiveBack.dao.findFirst(sqlReceive);
				String next_check_date=sdf.format(frock.getNextCheckDate());
				int status=frock.getStatus();
				if (status==0) {
					rd.set("status_name", "可使用");
				}else if (status==1) {
					rd.set("status_name", "封存");
				}else if (status==2) {
					rd.set("status_name", "待修");
				}
				rd.set("code", 0);
				rd.set("msg", "获取刀具信息成功！");
				rd.set("data", frock);
				rd.set("next_check_date", next_check_date);
				rd.set("receive", receive);
			}
		}
		renderJson(rd);
	}
	//处理归还
	public void doBack() {
		long receiveId=getParaToLong("receiveId");
		String userId=get("userId");
		Record jsp=new Record();
		if (userId==null||"".equals(userId)) {
			jsp.set("code", 200);
			jsp.set("msg", "员工ID有误，请确认！");
		}else {
			String userName=get("userName");
			String manager_user_id=get("manager_user_id");
			String manager_user_name=get("manager_user_name");
			long usedLife=getParaToLong("used_life");
			Date date=new Date();
			ReceiveBack back=ReceiveBack.dao.findById(receiveId);
			back.setStatus(3)
			.setBackUserId(userId).setBackUserName(userName)
			.setBackSpUserId(manager_user_id).setBackSpUserName(manager_user_name)
			.setBackDate(date)
			.setUsedLife(usedLife);
			back.update();
			//给工装管理员审批发送信息
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(manager_user_id, "工装退还通知：\n"+userName+" 发起工装退还审批，请登陆系统审批！"+sdf.format(date));
			jsp.set("code", 0);
			jsp.set("msg", "等待审批！");
		}
		renderJson(jsp);
	}
	//获取待审批列表
	public void getNewBackSp() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlReceive="select a.*,b.barcode,b.frock_name,b.frock_barcode "
				+ "from receive_back a,frocks b where a.status=3 and b.id=a.frock_id and a.back_sp_user_id='"+user.getUserid()+"'";
		List<Record> receiveSpList=Db.use("dc").find(sqlReceive);
		Record jsp=new Record();
		if (receiveSpList.isEmpty()) {
			jsp.set("code", 1);
		}else {
			jsp.set("code", 0);
		}
		jsp.set("data", receiveSpList);
		jsp.set("msg", "获取待审批列表成功！");
		renderJson(jsp);
	}
	//审批通过
	public void spPassNewFrock() {
		long receiveId=getParaToLong("receiveId");
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		Record jsp=new Record();
		jsp.set("code", 0);
		if (receive.getStatus()==3) {
			receive.setStatus(4);
			receive.update();
			long usedLife=receive.getUsedLife();
			//基本信息表在库数量增加
			Frocks frock=Frocks.dao.findById(receive.getFrockId());
			long oldUsedLife=frock.getUsedLife();
			long newUsedLife=usedLife+oldUsedLife;//新的使用寿命
			int instoreNum=frock.getInstoreNum();
			int receiveNum=receive.getReceiveNum();
			int newInstoreNum=instoreNum+receiveNum;
			frock.setInstoreNum(newInstoreNum)
			.setUsedLife(newUsedLife);
			frock.update();
			jsp.set("msg", "审批成功！");
			Date date=new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(receive.getBackUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】归还，已审批通过，请知悉！"+sdf.format(date));
		}else {
			jsp.set("msg", "请勿重复操作！");
		}
		jsp.set("data", "");
		renderJson(jsp);
	}
	//审批驳回
	public void spBackNewFrock() {
		long receiveId=getParaToLong("receiveId");
		ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
		Record jsp=new Record();
		jsp.set("code", 0);
		if (receive.getStatus()==3) {
			receive.setStatus(1);
			receive.update();
			Frocks frock=Frocks.dao.findById(receive.getFrockId());
			
			Date date=new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(receive.getBackUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】归还，被审批驳回，请知悉！"+sdf.format(date));
			jsp.set("msg", "审批成功！");
		}else {
			jsp.set("msg", "请勿重复操作！");
		}
		jsp.set("data", "");
		renderJson(jsp);
	}
}
