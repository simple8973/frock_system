package com.simple.controller.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KDelivery;
import com.simple.common.model.KErpRecords;
import com.simple.common.model.KMaterialList;
import com.simple.common.model.KPalletList;
import com.simple.common.model.UserRole;
/**
 * 手机端扫码收货  appReceiveGoods/getScancode?barcode='20131210604009'
 * @author FL00024996
 *
 */
public class AppReceiveGoodsController extends Controller {
	/**
	 * 扫码获取SRM送货信息
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月7日 上午9:42:31
	 */
	public void getScancode() {
		String barcode=get("barcode");
		//String barcode="20131210604009";
		String result = HttpKit.post("http://srm.fulinpm.com:58060/fulin/srm/service/deliveryInterface", "{delivery_no:'"+barcode+"'}");
		renderJson(result);
	}
	/**
	 * 确认收货
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月7日 下午3:41:26
	 */
	@SuppressWarnings("rawtypes")
	public void doReceiveGoods() {
		Record resp=new Record();
		Date date=new Date();
		Map line=FastJson.getJson().parse(get("line"), Map.class);
		Map head=FastJson.getJson().parse(get("head"), Map.class); 
		String userId=get("userId");
		String userName=get("userName");
		JSONArray palletList=JSONArray.parseArray(line.get("pallet_list").toString());
		try {
			boolean flag = Db.tx(() -> {
				//判断收货物料数量与托/箱数量是否一致
				int materialNum=Integer.valueOf(line.get("qty").toString());
				int boxNum=0;
				for (int i = 0; i < palletList.size(); i++) {
					JSONObject palletObject=palletList.getJSONObject(i);
					boolean check=palletObject.getBooleanValue("check");
					int pallet_statu=palletObject.getIntValue("pallet_statu");
					if (check&&pallet_statu==0) {
						boxNum=boxNum+palletObject.getIntValue("pallet_qty");
					}
				}
				if (boxNum!=materialNum) {
					resp.set("code", 500);
					resp.set("msg", "收货失败，选择的托/箱总数量不等于物料数量！");
					return false;
				}else {
					//回写SRM收货状态
					Record srm = new Record();
					srm.set("sign", "d28e8914f95247b4842a91231e");
					List<Record> list = new ArrayList<>();
					for (int i = 0; i < palletList.size(); i++) {
						JSONObject palletTemp = palletList.getJSONObject(i);
						if(palletTemp.getBoolean("check") && palletTemp.getInteger("pallet_statu")==0){
							Record one = new Record();
							one.set("delivery_no", head.get("delivery_no").toString());
							one.set("line_no", line.get("line_no").toString());
							one.set("pallet_no", palletTemp.get("pallet_no").toString());
							one.set("receive_num", palletTemp.getIntValue("pallet_qty"));
							list.add(one);
						}
					}
					srm.set("data", list);
					String srmStatu = HttpKit.post("http://srm.fulinpm.com:58060/fulin/srm/service/receiptsInterface", srm.toJson());
					JSONObject jb = JSONObject.parseObject(srmStatu);
					JSONArray ja = jb.getJSONArray("data");
					System.err.println(ja);
					if(!"1".equals(ja.getJSONObject(0).getString("flag"))){
						//SRM报错记录
						KErpRecords srmRecord=new KErpRecords();
						srmRecord.setReason("回写SRM收货状态失败")
			    		.setCreateTime(new Date())
			    		.setInfo(line.toString()+"SRM回改状态");
						srmRecord.save();
						resp.set("code", 500);
						resp.set("msg", "收货失败，回写SRM收货状态失败！");
						return false;
					}else {
						//是否库房
						UserRole userRole=UserRole.dao.findFirst("select * from user_role where role_id=5 and user_id='"+userId+"'");
						int frockType=0;
						if (userRole==null) {
							frockType=9;//不确定是否易损件
						}
						//新增送货单信息
						KDelivery delivery=KDelivery.dao.findFirst("select * from k_delivery where delivery_no='"+head.get("delivery_no").toString()+"'");
						if (delivery==null) {
							delivery=new KDelivery();
							delivery.setDeliveryNo(head.get("delivery_no").toString());
							delivery.setSupplierNo(head.get("supplier_no").toString());
							delivery.setSupplierName(head.get("supplier_name").toString());
							delivery.setDeliverLbn(head.get("deliver_lbn").toString());
							delivery.save();
						}
						//新增物料列表
						KMaterialList material=KMaterialList.dao.findFirst("select * from k_material_list where "
								+ "delivery_id="+delivery.getId()+" and order_no='"+line.get("order_no").toString()+"' and "
								+ "order_line_no='"+line.get("order_line_no").toString()+"' and part_no='"+line.get("part_no").toString()+"' and "
								+ "qty="+Integer.valueOf(line.get("qty").toString()));
						if (material==null) {
							material=new KMaterialList();
							material.setDeliveryId(delivery.getId())
							.setLineNo(Integer.valueOf(line.get("line_no").toString()))
							.setOrderNo(line.get("order_no").toString())
							.setOrderLineNo(Integer.valueOf(line.get("order_line_no").toString()))
							.setPartNo(line.get("part_no").toString())
							.setPartDesc(line.get("part_desc").toString())
							.setLotBatchNo(line.get("lot_batch_no").toString())
							.setQty(Integer.valueOf(line.get("qty").toString()))
							.setBox(line.get("box").toString())
							.setPallet(line.get("pallet").toString())
							.setRemaining(line.get("remaining").toString())
							.setReceiveUserId(userId).setReceiveUserName(userName)
							.setReceiveDate(date)//收货日期
							.setFrockType(frockType);//0-技术中心；9-工装管理员收货
							material.save();
						}
						for (int i = 0; i < palletList.size(); i++) {
							JSONObject palletObject=palletList.getJSONObject(i);
							boolean check=palletObject.getBooleanValue("check");
							int pallet_statu=palletObject.getIntValue("pallet_statu");
							if (check&&pallet_statu==0) {
								//新增托盘数据
								KPalletList pallet=KPalletList.dao.findFirst("select * from k_pallet_list where "
										+ "material_id="+material.getId()+" and pallet_no='"+palletObject.get("pallet_no")+"' and pallet_qty="+palletObject.getIntValue("pallet_qty"));
								if (pallet==null) {
									pallet=new KPalletList();
									pallet.setMaterialId(material.getId())
									.setPalletNo(palletObject.get("pallet_no").toString())
									.setPalletQty(palletObject.getIntValue("pallet_qty"));
									pallet.save();
								}
								//修改状态
								palletList.getJSONObject(i).replace("pallet_statu", 1);
							}
						}
						resp.set("pallet_list", palletList);
						resp.set("code", 0);
						resp.set("msg", "收货成功！");
						return true;
					}
				}
			});
			if (flag) {
				renderJson(resp);
			}else {
				renderJson(Ret.fail("msg", resp.getStr("msg")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			KErpRecords erpRecord=new KErpRecords();
    		erpRecord.setReason(e.getMessage())
    		.setCreateTime(new Date())
    		.setInfo(line.toString()+"扫码收货");
    		erpRecord.save();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
