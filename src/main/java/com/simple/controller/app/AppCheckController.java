package com.simple.controller.app;

import java.util.Calendar;
import java.util.Date;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.CheckRecord;
import com.simple.common.model.Frocks;
/**
 * 校验appCheck
 * @author FL00024996
 *
 */
public class AppCheckController extends Controller {
	//处理确认校验
	public void doCheck() {
		String userId=get("userId");
		String userName=get("userName");
		long frockId=getParaToLong("frockId");
		int checkCycle=getParaToInt("check_cycle");
		int checkResultIndex=getParaToInt("checkResultIndex");
		int statusIndex=getParaToInt("statusIndex");
		String checkRemarks=get("check_remarks");
		//校验表新增数据
		Date date=new Date(); 
		CheckRecord checkRecord=getModel(CheckRecord.class);
		checkRecord.setFrockId(frockId)
		.setCheckUserId(userId).setCheckUserName(userName)
		.setCheckResult(checkResultIndex)
		.setCheckDate(date)
		.setCheckCycle(checkCycle)
		.setCheckRemarks(checkRemarks);
		checkRecord.save();
		//获取工装当前有效期，得到新有效期
		Frocks frock=Frocks.dao.findById(frockId);
		Calendar calendar=Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, checkCycle);
		Date newYouxiaoqiDate=calendar.getTime();
		frock.setNextCheckDate(newYouxiaoqiDate)
		.setStatus(statusIndex);
		frock.update();
		
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("msg", "校验成功！");
		renderJson(jsp);
	}
}
