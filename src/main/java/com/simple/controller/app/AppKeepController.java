package com.simple.controller.app;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.KeepRecord;

/**
 * app保养appKeep
 *
 * @author FL00024996
 */
public class AppKeepController extends Controller {
    //保养处理（提交保养）
    public void doKeep() {
        long frockId = getParaToLong("frockId");
        String keepRemarks = get("keep_remarks");
        long productionQuantity=getParaToLong("production_quantity");  //从前端获取生产数量
        String userId = get("userId");
        String userName = get("userName");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setTime(date);
        int dayWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        if (dayWeek == 1) {//是否星期天
            //是否新年第一周
            if (week == 1) {
                Calendar lastWeek = Calendar.getInstance();
                lastWeek.add(Calendar.DAY_OF_YEAR, -7);
                int lastYearWeeks = lastWeek.get(Calendar.WEEK_OF_YEAR);
                week = lastYearWeeks;
            } else {
                week = week - 1;
            }
        }

        Record jsp = new Record();

        //先将上一条改为旧纪录
        List<KeepRecord> oldKeepRecords = KeepRecord.dao.find("select * from keep_record where frock_id=" + frockId + " and status=0");
        if (!oldKeepRecords.isEmpty()) {
            KeepRecord oldKeepRecord = oldKeepRecords.get(0);
            oldKeepRecord.setStatus(1);
            oldKeepRecord.update();
        }
        //新增记录
        KeepRecord keepRecord = getModel(KeepRecord.class);
        keepRecord.setFrockId(frockId)
                .setKeepUserId(userId).setKeepUserName(userName)
                .setKeepDate(date)
                .setKeepRemarks(keepRemarks)
                .setKeepWeek(week)
                .setProductionQuantity(productionQuantity)
                .setStatus(0);
        keepRecord.save();
        jsp.set("code", 0);
        jsp.set("msg", "保养成功！");
        renderJson(jsp);
    }

}
