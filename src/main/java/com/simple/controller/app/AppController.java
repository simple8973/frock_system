package com.simple.controller.app;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.request.OapiUserGetuserinfoRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserGetuserinfoResponse;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.config.URLConstant;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

//E应用端处理类
public class AppController extends Controller{
	//开发测试用
	public void test() throws ApiException{ 
		String userId="FL00001334";
		String dingtoken = AccessTokenUtil.getToken();
		//获取用户详情
		DingTalkClient client3 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid(userId);//FL00000194 guo 30512532441289105
		request.setHttpMethod("GET");
		OapiUserGetResponse response3 = client3.execute(request, dingtoken);
		
		UsernamePasswordToken token = new UsernamePasswordToken(userId, userId);
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		subject.getSession().setAttribute("user", response3);
		Record rsp=new Record();
		JSONObject jb = JSONObject.parseObject(response3.getBody());
		rsp.set("djmanager", 0);
		rsp.set("userinfo", jb.toString());
		rsp.set("code", 0);
		rsp.set("msg", "登陆成功");
        renderJson(rsp);
	}
	//钉钉E应用免登
	public void login() throws ApiException{
		//获取access_token
		String accessToken=AccessTokenUtil.getToken();
		DingTalkClient client=new DefaultDingTalkClient(URLConstant.URL_GET_USER_INFO);
		OapiUserGetuserinfoRequest request=new OapiUserGetuserinfoRequest();
		request.setCode(getPara("authCode"));
		request.setHttpMethod("GET");
		
		OapiUserGetuserinfoResponse response=null;
		try {
			response=client.execute(request,accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
			renderNull();
		}
		//查询得到当前用户的userId
		//获取得到userId之后应用应该处理自身的登陆绘画管理（session），避免后续业务交互（前端到应用服务端）每次都要重新获取用户身份，提升用户体验
		String userId=response.getUserid();
		DingTalkClient client2= new DefaultDingTalkClient(URLConstant.URL_USER_GET);
		OapiUserGetRequest request2=new OapiUserGetRequest();
		request2.setUserid(userId);
		request2.setHttpMethod("GET");
		
		OapiUserGetResponse userinfo = null;
		try {
			userinfo = client2.execute(request2, accessToken);
		} catch (ApiException e) {
			e.printStackTrace();
			renderNull();
		}
		UsernamePasswordToken token = new UsernamePasswordToken(userId, userId);
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		subject.getSession().setAttribute("user", userinfo);
		
        Record rsp=new Record();
		JSONObject jb = JSONObject.parseObject(userinfo.getBody());
		String sql_djManager="select * from user_role where user_id='"+userId+"' and role_id=1";
		List<Record> djManagderList=Db.use("dc").find(sql_djManager);
		String sqlBanzhang="select * from user_role where user_id='"+userId+"' and role_id=3 ";
		List<Record> banzhangList=Db.use("dc").find(sqlBanzhang);
		String sqlKuFang="select * from user_role where user_id='"+userId+"' and role_id=5 ";
		List<Record> kuFangList=Db.use("dc").find(sqlKuFang);
		//判断是否采购员
		if (!banzhangList.isEmpty()) {
			rsp.set("djmanager", 2);//是班长
		}else if (!djManagderList.isEmpty()) {
			rsp.set("djmanager", 1);//是工装管理员
		}else if (!kuFangList.isEmpty()) {
			rsp.set("djmanager", 5);//库房
		}else {
			rsp.set("djmanager", 0);//普通用户
		}
		rsp.set("userinfo", jb.toString());
		rsp.set("code", 0);
		rsp.set("msg", "登陆成功");
        renderJson(rsp);
	}
	//获取用户详情
	@NotAction
	private OapiUserGetResponse getUserInfo(String accessToken,String userId) {
		try {
			DingTalkClient client =new DefaultDingTalkClient(URLConstant.URL_USER_GET);
			OapiUserGetRequest request=new OapiUserGetRequest();
			request.setUserid(userId);
			request.setHttpMethod("GET");
			OapiUserGetResponse response=client.execute(request,accessToken);
			return response;
		}catch(ApiException e) {
			e.printStackTrace();
			return null;
		}
	}

}
