package com.simple.controller.app;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * frockInfo
 */
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.Frocks;

public class AppFrockController extends Controller {
	//扫码获取工装基本信息
	public void getFrockInfo() {
		String barcode=get("barcode");
		String sqlFrock="select * from frocks where barcode='"+barcode+"'";
		List<Frocks> frocks=Frocks.dao.find(sqlFrock);
		Record rd = new Record();
		if (frocks.isEmpty()) {
			rd.set("code", 1);
			rd.set("msg", "未查询到当前工装基本信息！");
		}else {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Frocks frock=Frocks.dao.findFirst(sqlFrock);
			String arrival_date=sdf.format(frock.getArrivalDate());
			String next_check_date=sdf.format(frock.getNextCheckDate());
			int status=frock.getStatus();
			if (status==0) {
				rd.set("status_name", "可使用");
			}else if (status==1) {
				rd.set("status_name", "封存");
			}else if (status==2) {
				rd.set("status_name", "待修");
			}
			//查询领用情况
			List<Record> receives=Db.use("dc").find("select * from receive_back where frock_id="+frock.getId()+" and status=1");
			for (int i = 0; i < receives.size(); i++) {
				Date receiveDate=receives.get(i).getDate("receive_date");
				receives.get(i).set("receive_date", sdf.format(receiveDate));
			}
			rd.set("code", 0);
			rd.set("msg", "获取刀具信息成功！");
			rd.set("data", frock);
			rd.set("next_check_date", next_check_date);
			rd.set("arrival_date", arrival_date);
			rd.set("receives", receives);
		}
		renderJson(rd);
	}
}
