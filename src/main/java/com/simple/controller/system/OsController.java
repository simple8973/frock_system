package com.simple.controller.system;

import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.UserWare;
import com.simple.common.model.WareHouse;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class OsController extends Controller{
	//跳转组织架构
	public void index() {
		render("os.html");
	}
	
	//查看员工详细信息
	public void userinfo() throws ApiException{
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid(getPara(0));
		request.setHttpMethod("GET");
		OapiUserGetResponse response = client.execute(request, AccessTokenUtil.getToken());
		setAttr("user",response);
		//已设置的角色
		Record roles=Db.use("dc").findFirst("SELECT GROUP_CONCAT(role_id) role_ids FROM user_role WHERE user_id = '" + response.getUserid()+"'");
		setAttr("roleIds", roles.get("role_ids"));
		//当前员工所属车间
		UserWare userWares=UserWare.dao.findFirst("select GROUP_CONCAT(ware_id) ware_ids from user_ware where user_id='"+response.getUserid()+"' and status=0");
		set("userWareIds", userWares.get("ware_ids"));
		render("userinfo.html");
	}
	//获取员工车间列表
	public void getWares() {
		List<WareHouse> wareHouses=WareHouse.dao.find("select * from ware_house");
		renderJson(wareHouses);
	}
}
