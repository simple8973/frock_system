package com.simple.controller.frock;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.FrockRepair;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.ding.DingUtil;
import com.simple.excelModel.RepaireExportModel;
/**
 * 报修/repaire/roRepairList
 * @author FL00024996
 *
 */
public class RepairController extends Controller {
	//指向报修列表页面
	public void roRepairList() {
		render("repairList.html");
	}
	//获取报修工装列表
	public void getRepairList() {
		String barcode=get("barcode");
		String frockName=get("frockName");
		String sqUser=get("sqUser");
		String bzUser=get("bzUser");
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		//角色
		String sqlRole="select * from user_role where user_id='"+user.getUserid()+"' and role_id=2";
		List<Record> roleList=Db.use("dc").find(sqlRole);
		int isAdmin=0;
		if (roleList.size()>0) {
			isAdmin=1;
		}
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		if (isAdmin==1) {//超级管理员			
			sb.append(" from frocks a,frock_repair b where b.frock_id=a.id ");
		}else {
			sb.append(" from frocks a,frock_repair b where b.frock_id=a.id and b.manager_user_id='"+user.getUserid()+"' ");			
		}
		if (!"".equals(barcode)&&barcode!=null) {
			sb.append(" and a.barcode like '%"+barcode+"%' ");
		}
		if (!"".equals(frockName)&&frockName!=null) {
			sb.append(" and a.frock_name like '%"+frockName+"%' ");
		}
		String frockBarcode=get("frockBarcode");
		if (!"".equals(frockBarcode)&&frockBarcode!=null) {
			sb.append(" and a.frock_barcode like '%"+frockBarcode+"%' ");
		}
		
		if (!"".equals(sqUser)&&sqUser!=null) {
			sb.append(" and b.sq_user_name like '%"+sqUser+"%' ");
		}
		if (!"".equals(bzUser)&&bzUser!=null) {
			sb.append(" and b.bz_user_name like '%"+bzUser+"%' ");
		}
		String statusString=get("repairStatus");
		if (statusString!=null&&!"".equals(statusString)) {
			int repairStatus=getParaToInt("repairStatus");
			if (repairStatus!=10) {
				sb.append(" and b.status="+repairStatus);
			}else {
				sb.append(" and b.status<>9 ");
			}
		}
		Page<Record> repairList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.barcode,a.frock_name,a.frock_barcode ",sb.toString()+" order by b.id desc ");
		Record jsp=new Record();
		jsp.set("count", repairList.getTotalRow());
		jsp.set("data", repairList.getList());
		jsp.set("msg", "获取报修列表成功");
		renderJson(jsp);
	}
	//开始维修
	public void subStartRepair() {
		try {
			long repairId=getParaToLong("repairId");
			String repairType=get("repairType");
			String reason=get("reason");
			Date date=new Date();
			//处理报修
			FrockRepair repair=FrockRepair.dao.findById(repairId);
			repair
			.setRepairType(repairType)
			.setHandDate(date);
			if ("报废".equals(repairType)) {//报废
				repair.setStatus(5)
				.setReason(reason).setFinishDate(date);
			}else {
				repair.setStatus(3);
			}
			repair.update();
			//工装表状态改变
			Frocks frock=Frocks.dao.findById(repair.getFrockId());
			long oldUsedLife=frock.getUsedLife();
			long newUsedLife=repair.getUsedLife()+oldUsedLife;//新的使用寿命
			frock.setUsedLife(newUsedLife);
			if ("报废".equals(repairType)) {//报废
				int totalNum=frock.getTotalNum();
				frock.setStatus(1)//封存
				.setInstoreNum(totalNum);
				frock.update();
			}else {
				frock.setStatus(2);//待修
				frock.update();
			}
			//领用表状态改变,改为已归还
			ReceiveBack receiveBack=ReceiveBack.dao.findFirst("select * from receive_back where frock_id ="+repair.getFrockId()+" and status=1 ");
			receiveBack.setStatus(4)//已归还
			.setUsedLife(repair.getUsedLife())//生产件数
			.setBackSpUserId(repair.getManagerUserId()).setBackSpUserName(repair.getManagerUserName())
			.setBackUserId(repair.getSqUserId()).setBackUserName(repair.getSqUserName())
			.setBackDate(date);
			receiveBack.update();
			
			renderJson(Ret.ok("msg", "处理成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//维修完成
	public void subFinishRepair() {
		try {
			long repairId=getParaToLong("repairId");
			String finishReason=get("finishReason");
			String resolution=get("resolution");
			Date date=new Date();
			//维修表
			FrockRepair repair=FrockRepair.dao.findById(repairId);
			repair.setStatus(4)
			.setReason(finishReason).setResolution(resolution)
			.setFinishDate(date);
			repair.update();
			//工装表状态
			Frocks frock=Frocks.dao.findById(repair.getFrockId());
			int totalNum=frock.getTotalNum();
			frock.setStatus(0)//可使用
			.setInstoreNum(totalNum);
			frock.update();
			//给申请人发送钉钉通知，可继续领用
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DingUtil.sendText(repair.getSqUserName(), "工装报修通知：你提交的\n工装编号："+frock.getBarcode()+"\n工装名称："+frock.getFrockName()+"\n已维修完成，可到管理员处领取使用！"+sdf.format(date));
			renderJson(Ret.ok("msg", "处理成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//报废工装
	public void subUselessRepair() {
		try {
			long repairId=getParaToLong("repairId");
			String uselessReason=get("uselessReason");
			Date date=new Date();
			//维修表
			FrockRepair repair=FrockRepair.dao.findById(repairId);
			repair.setStatus(5)
			.setReason(uselessReason)
			.setFinishDate(date);
			repair.update();
			//工装表
			Frocks frock=Frocks.dao.findById(repair.getFrockId());
			int totalNum=frock.getTotalNum();
			frock.setStatus(1)//封存
			.setInstoreNum(totalNum);
			frock.update();
			
			renderJson(Ret.ok("msg", "处理成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//导出
	public void exportRepaire() {
		String barcode=get("barcode");
		String frockName=get("frockName");
		String sqUser=get("sqUser");
		String bzUser=get("bzUser");
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlRepaire="select b.*,a.barcode,a.frock_name,a.frock_barcode from frocks a,frock_repair b "
				+ "where b.frock_id=a.id and b.manager_user_id='"+user.getUserid()+"' ";
		if (!"".equals(barcode)&&barcode!=null) {
			sqlRepaire=sqlRepaire+" and a.barcode like '%"+barcode+"%' ";
		}
		if (!"".equals(frockName)&&frockName!=null) {
			sqlRepaire=sqlRepaire+" and a.frock_name like '%"+frockName+"%' ";
		}
		if (!"".equals(sqUser)&&sqUser!=null) {
			sqlRepaire=sqlRepaire+" and b.sq_user_name like '%"+sqUser+"%' ";
		}
		if (!"".equals(bzUser)&&bzUser!=null) {
			sqlRepaire=sqlRepaire+" and b.bz_user_name like '%"+bzUser+"%' ";
		}
		String statusString=get("repairStatus");
		if (statusString!=null&&!"".equals(statusString)) {
			int repairStatus=getParaToInt("repairStatus");
			if (repairStatus!=10) {
				sqlRepaire=sqlRepaire+" and b.status="+repairStatus;
			}else {
				sqlRepaire=sqlRepaire+" and b.status<>9 ";
			}
		}
		List<Record> repaireList=Db.use("dc").find(sqlRepaire);
		Record req=new Record();
		Db.tx(() -> {
			try {
				List<RepaireExportModel> dataList = this.getDataList(repaireList);
				Workbook workbook = DefaultExcelBuilder.of(RepaireExportModel.class)
				         .build(dataList);
				FileExportUtil.export(workbook, new File("D:/FrockFujian/upload/moban/报修记录.xlsx"));
				req.set("code", 0);
				req.set("msg", "报修记录导出成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	public void downRepaireExcel() {
		Record rd=new Record();
		try {
			renderFile("/moban/报修记录.xlsx");
		} catch (Exception e) {
			rd.set("code", 1);
			rd.set("msg", "暂无！");
			renderJson(rd);
		}
	}
	public List<RepaireExportModel> getDataList(List<Record> frockList) {
		List<RepaireExportModel> dataList=new ArrayList<RepaireExportModel>(frockList.size());
	    for (int i = 0; i < frockList.size(); i++) {
	    	int status=frockList.get(i).getInt("status");
	    	String statusName="";
	    	if (status==0) {
	    		statusName="待审批";
			}else if (status==1) {
				statusName="待处理";
			}else if (status==2) {
				statusName="班长驳回";
			}else if (status==3) {
				statusName="维修中";
			}else if (status==4) {
				statusName="维修完成";
			}else {
				statusName="报废";
			}
	    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    	RepaireExportModel exportModel=new RepaireExportModel();
	    	exportModel.setBarcode(frockList.get(i).getStr("barcode"));
	    	exportModel.setFrock_name(frockList.get(i).getStr("frock_name"));
	    	exportModel.setFrock_barcode(frockList.get(i).getStr("frock_barcode"));
	    	exportModel.setSq_user_name(frockList.get(i).getStr("sq_user_name"));
	    	exportModel.setProblem(frockList.get(i).getStr("problem"));
	    	exportModel.setStatus(statusName);
	    	exportModel.setSq_date(sdf.format(frockList.get(i).getDate("sq_date")));
	    	exportModel.setBack_user_name(frockList.get(i).getStr("back_user_name"));
	    	exportModel.setReason(frockList.get(i).getStr("reason"));
	    	exportModel.setResolution(frockList.get(i).getStr("resolution"));
	    	Date hand_date=frockList.get(i).getDate("hand_date");
	    	if (hand_date!=null&&!"null".equals(hand_date)) {
	    		exportModel.setHand_date(sdf.format(frockList.get(i).getDate("hand_date")));
			}
	    	Date finish_date=frockList.get(i).getDate("finish_date");
	    	if (finish_date!=null&&!"null".equals(finish_date)) {
	    		exportModel.setFinish_date(sdf.format(frockList.get(i).getDate("finish_date")));
			}
	    	dataList.add(exportModel);
	    }
	    return dataList;
	}
}
