package com.simple.controller.frock;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.FrockEditRecord;
import com.simple.common.model.Frocks;
import com.simple.common.model.Fujians;
import com.simple.common.model.WareHouse;
import com.simple.excelModel.FrockExportModel;

/**
 * 工装
 *
 * @author FL00024996
 */
public class FrockController extends Controller {
    //指向工装列表页面
    public void toFrockList() {
        render("frockList.html");
    }

    public void getFrockList() {
        OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
        String sqlAdmin = "select * from user_role where user_id='" + user.getUserid() + "' and (role_id=2 or role_id=4)";//admin、工装领导
        int isAdmin = 0;//0-非admin\工装领导
        List<Record> adminList = Db.use("dc").find(sqlAdmin);
        if (!adminList.isEmpty()) {
            isAdmin = 1;
        }
        String sqlRole = "select * from user_role where user_id='" + user.getUserid() + "' and role_id=1";//工装管理员、admin
        List<Record> roleList = Db.use("dc").find(sqlRole);
        int isManager = 0;
        if (roleList.size() > 0) {
            isManager = 1;
        }
        String createUser = get("createUser");
        String frockBianhao = get("frockBianhao");
        String frockName = get("frockName");
        String frockBarcode = get("frockBarcode");
        String frockProduct = get("frockProduct");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append(" from frocks  where 1=1 ");
        if (isAdmin == 0) {//非系统管理员
            if (isManager == 1) { //工装管理员
                sb.append(" and ware_id in (select ware_id from user_ware where user_id='" + user.getUserid() + "' and status=0)");
            } else {
                sb.append(" and create_user_id='" + user.getUserid() + "' ");
            }
        }

        if (!"".equals(createUser) && createUser != null) {
            sb.append(" and create_user_name like '%" + createUser + "%' ");
        }
        if (!"".equals(frockBianhao) && frockBianhao != null) {
            sb.append(" and barcode like '%" + frockBianhao + "%' ");
        }
        if (!"".equals(frockName) && frockName != null) {
            sb.append(" and frock_name like '%" + frockName + "%' ");
        }
        if (!"".equals(frockBarcode) && frockBarcode != null) {
            sb.append(" and frock_barcode like '%" + frockBarcode + "%' ");
        }
        if (!"".equals(frockProduct) && frockProduct != null) {
            sb.append(" and product like '%" + frockProduct + "%' ");
        }
        Page<Record> feockList = Db.use("dc").paginate(currentPage, pageSize, "select * ", sb.toString());
        //领用中的工装不能编辑
        for (int i = 0; i < feockList.getList().size(); i++) {
            int totalNum = feockList.getList().get(i).getInt("total_num");
            int inStoreNum = feockList.getList().get(i).getInt("instore_num");
            if (totalNum == inStoreNum) {
                feockList.getList().get(i).set("canEdit", 0);
            } else {
                feockList.getList().get(i).set("canEdit", 1);
            }

            //剩余寿命
            BigDecimal theoryLife = new BigDecimal(feockList.getList().get(i).getStr("theory_life"));
            BigDecimal usedLife = new BigDecimal(feockList.getList().get(i).getStr("used_life"));
            BigDecimal surplusLife = (theoryLife.subtract(usedLife)).divide(new BigDecimal(10000));


            feockList.getList().get(i).set("surplus_life", surplusLife);
            feockList.getList().get(i).set("theory_life", theoryLife.divide(new BigDecimal(10000)));
        }
        Record jsp = new Record();
        jsp.set("count", feockList.getTotalRow());
        jsp.set("data", feockList.getList());
        jsp.set("msg", "获取工装列表成功");
        jsp.set("role", isManager);
        jsp.set("loginuser", user.getUserid());
        renderJson(jsp);
    }

    @SuppressWarnings("rawtypes")
    public void addFrock() {
        try {
            OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Map map = FastJson.getJson().parse(get("formData"), Map.class);
            String wareId = map.get("wareHouseId").toString();
            String barcode = map.get("barcode").toString().trim();
            //寿命
            double life = Double.parseDouble(map.get("theory_life").toString());
            double dbLife = Math.round(life * 10000);
            long theoryLife = new Double(dbLife).longValue();

            //判断编号是否重复
            String sqlFrock = "select * from frocks where barcode='" + barcode + "'";
            List<Record> haveFrockList = Db.use("dc").find(sqlFrock);
            if (haveFrockList.isEmpty()) {//没有重复
                Frocks frock = getModel(Frocks.class);
                String fileList = get("fileList");
                JSONArray fileArray = JSONArray.parseArray(fileList);
                JSONObject fileObject = fileArray.getJSONObject(0);
                long fileId = fileObject.getLongValue("id");

                frock.setWareId(Integer.valueOf(wareId))
                        .setBarcode(barcode)
                        .setIfsBarcode(map.get("ifs_barcode").toString())
                        .setFrockName(map.get("frock_name").toString())
                        .setFrockBarcode(map.get("frock_barcode").toString())
                        .setProduct(map.get("product").toString())
                        .setProduce(map.get("produce").toString())
                        .setProduceName(map.get("produce_name").toString())
                        .setTotalNum(Integer.valueOf(map.get("total_num").toString())).setInstoreNum(Integer.valueOf(map.get("total_num").toString()))
                        .setUnit(map.get("unit").toString())
                        .setTechUserId(map.get("tech_user_id").toString()).setTechUserName(map.get("tech_user_name").toString())
                        .setInformUserId(map.get("inform_user_id").toString()).setInformUserName(map.get("inform_user_name").toString())
                        .setArrivalDate(sdf.parse(map.get("arrival_date").toString()))
                        .setLocation(map.get("location").toString())
                        .setKeepContent(map.get("keep_content").toString())
                        .setKeepMethod(map.get("keep_method").toString()).setNextCheckDate(sdf.parse(map.get("next_check_date").toString()))
                        .setStatus(Integer.valueOf(map.get("status").toString()))
                        .setCreateUserId(user.getUserid()).setCreateUserName(user.getName())
                        .setAcceptId(fileId)
                        .setTheoryLife(theoryLife)
                        .setKeepDays(Integer.parseInt(map.get("keep_days").toString()));
                frock.save();
                //操作记录历史
                FrockEditRecord editRecord = new FrockEditRecord();
                editRecord.setFrockCode(barcode).setUserId(user.getUserid())
                        .setContent(map.toString()).setCreateTime(new Date());
                editRecord.save();

                renderJson(Ret.ok("msg", "新增成功！"));
            } else {
                renderJson(Ret.fail("error", "工装编号已存在，请重新填写"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("error", e));
        }
    }

    //修改工装后点击保存执行此方法
    @SuppressWarnings("rawtypes")
    public void editFrock() {
        try {
            OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            long editId = getParaToLong("editId");
            //判断是否重复
            Map map = FastJson.getJson().parse(get("formData"), Map.class);
            //寿命
            double life = Double.parseDouble(map.get("theory_life").toString());
            double dbLife = Math.round(life * 10000);
            long theoryLife = new Double(dbLife).longValue();
            String wareId = map.get("wareHouseId").toString();
            String barcode = map.get("barcode").toString().trim();
            String sqlFrock = "select * from frocks where barcode='" + barcode + "' and id<>" + editId;
            List<Record> haveFrockList = Db.use("dc").find(sqlFrock);
            if (haveFrockList.isEmpty()) {
                Frocks frock = Frocks.dao.findById(editId);
                String fileList = get("fileList");
                JSONArray fileArray = JSONArray.parseArray(fileList);
                JSONObject fileObject = fileArray.getJSONObject(0);
                long fileId = fileObject.getLongValue("id");
                frock.setWareId(Integer.valueOf(wareId))
                        .setBarcode(barcode)
                        .setIfsBarcode(map.get("ifs_barcode").toString())
                        .setFrockName(map.get("frock_name").toString())
                        .setFrockBarcode(map.get("frock_barcode").toString())
                        .setProduct(map.get("product").toString())
                        .setProduce(map.get("produce").toString())
                        .setProduceName(map.get("produce_name").toString())
                        .setTotalNum(Integer.valueOf(map.get("total_num").toString())).setInstoreNum(Integer.valueOf(map.get("total_num").toString()))
                        .setUnit(map.get("unit").toString())
                        .setTechUserId(map.get("tech_user_id").toString()).setTechUserName(map.get("tech_user_name").toString())
                        .setInformUserId(map.get("inform_user_id").toString()).setInformUserName(map.get("inform_user_name").toString())
                        .setArrivalDate(sdf.parse(map.get("arrival_date").toString()))
                        .setLocation(map.get("location").toString())
                        .setKeepContent(map.get("keep_content").toString())
                        .setKeepMethod(map.get("keep_method").toString()).setNextCheckDate(sdf.parse(map.get("next_check_date").toString()))
                        .setStatus(Integer.valueOf(map.get("status").toString()))
                        .setCreateUserId(user.getUserid()).setCreateUserName(user.getName())
                        .setAcceptId(fileId)
                        .setTheoryLife(theoryLife)
                        .setKeepDays(Integer.parseInt(map.get("keep_days").toString()));
                frock.update();
                //操作记录历史
                FrockEditRecord editRecord = new FrockEditRecord();
                editRecord.setFrockCode(barcode).setUserId(user.getUserid())
                        .setContent(map.toString()).setCreateTime(new Date());
                editRecord.save();

                renderJson(Ret.ok("msg", "编辑成功！"));
            } else {
                renderJson(Ret.fail("error", "工装编号已存在，请重新输入"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("error", e));
        }
    }

    public void getProductInfo() {//工装使用产品、工站、状态
        Record record = new Record();
        record.set("code", 0);
        long frockId = getParaToLong("frockId");
        Frocks frocks = Frocks.dao.findById(frockId);
        int status = frocks.getStatus();
        if (status == 0) {
            record.set("status_name", "可使用");
        } else if (status == 1) {
            record.set("status_name", "封存");
        } else if (status == 2) {
            record.set("status_name", "待修");
        }
        renderJson(record);
    }

    //打印
    public void toPrintFrock() {
        long frockId = getParaToLong(0);
        Frocks frock = Frocks.dao.findById(frockId);
        int status = frock.getStatus();
        if (status == 0) {
            set("status_name", "可使用");
        } else if (status == 1) {
            set("status_name", "封存");
        } else if (status == 2) {
            set("status_name", "待修");
        }
        set("frockInfo", frock);
        render("printFrock.html");
    }

    //获取领用记录
    public void getReceiveList() {
        long frockId = getParaToLong("frockId");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append("from receive_back where frock_id=" + frockId + " and status<>9 ");
        Page<Record> receiveList = Db.use("dc").paginate(currentPage, pageSize, "select * ", sb.toString() + " order by id desc");
        Record jsp = new Record();
        jsp.set("count", receiveList.getTotalRow());
        jsp.set("data", receiveList.getList());
        jsp.set("msg", "获取领用成功");
        renderJson(jsp);
    }

    //获取校验记录
    public void getCheckList() {
        long frockId = getParaToLong("frockId");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append("from check_record where frock_id=" + frockId);
        Page<Record> receiveList = Db.use("dc").paginate(currentPage, pageSize, "select * ", sb.toString() + " order by id desc");
        Record jsp = new Record();
        jsp.set("count", receiveList.getTotalRow());
        jsp.set("data", receiveList.getList());
        jsp.set("msg", "获取校验记录成功");
        renderJson(jsp);
    }

    //保养记录
    public void getKeepList() {
        long frockId = getParaToLong("frockId");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append("from keep_record where frock_id=" + frockId);
        Page<Record> receiveList = Db.use("dc").paginate(currentPage, pageSize, "select * ", sb.toString() + " order by id desc");
        Record jsp = new Record();
        jsp.set("count", receiveList.getTotalRow());
        jsp.set("data", receiveList.getList());
        jsp.set("msg", "获取保养记录成功");
        renderJson(jsp);
    }

    //报修记录
    public void getRepairList() {
        long frockId = getParaToLong("frockId");
        int currentPage = getParaToInt("currentPage");
        int pageSize = getParaToInt("pageSize");
        StringBuilder sb = new StringBuilder();
        sb.append("from frock_repair where frock_id=" + frockId + " and status<>9");
        Page<Record> repairList = Db.use("dc").paginate(currentPage, pageSize, "select * ", sb.toString() + " order by id desc");
        Record jsp = new Record();
        jsp.set("count", repairList.getTotalRow());
        jsp.set("data", repairList.getList());
        jsp.set("msg", "获取报修记录成功");
        renderJson(jsp);
    }

    /**
     * 公共
     */
    public void getWareList() {
        List<WareHouse> wareList = WareHouse.dao.find("select * from ware_house");
        Record jsp = new Record();
        jsp.set("data", wareList);
        jsp.set("msg", "获取车间成功");
        renderJson(jsp);
    }

    //获取验收文件
    public void getAcceptFujian() {
        long frockId = getParaToLong("frockId");
        Frocks frock = Frocks.dao.findById(frockId);
        Fujians acceptFujians = Fujians.dao.findById(frock.getAcceptId());
        Record record = new Record();
        record.set("data", acceptFujians);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    //导出工装列表
    public void exportFrock() {
        String createUser = get("createUser");
        String frockBianhao = get("frockBianhao");
        String frockName = get("frockName");
        String frockBarcode = get("frockBarcode");
        String frockProduct = get("frockProduct");
        String sqlFrock = "select *,(select a.part_no from k_material_list a where a.id=material_id) as ifs_part_no from frocks where 1=1 ";
        if (!"".equals(createUser) && createUser != null) {
            sqlFrock = sqlFrock + " and create_user_name like '%" + createUser + "%' ";
        }
        if (!"".equals(frockBianhao) && frockBianhao != null) {
            sqlFrock = sqlFrock + " and barcode like '%" + frockBianhao + "%' ";
        }
        if (!"".equals(frockName) && frockName != null) {
            sqlFrock = sqlFrock + " and frock_name like '%" + frockName + "%' ";
        }
        if (!"".equals(frockBarcode) && frockBarcode != null) {
            sqlFrock = sqlFrock + " and frock_barcode like '%" + frockBarcode + "%' ";
        }
        if (!"".equals(frockProduct) && frockProduct != null) {
            sqlFrock = sqlFrock + " and product like '%" + frockProduct + "%' ";
        }
        List<Frocks> frockList = Frocks.dao.find(sqlFrock);
        Record req = new Record();
        Db.tx(() -> {
            try {
                List<FrockExportModel> dataList = this.getDataList(frockList);
                Workbook workbook = DefaultExcelBuilder.of(FrockExportModel.class)
                        .build(dataList);
                FileExportUtil.export(workbook, new File("D:/FrockFujian/upload/moban/工装列表.xlsx"));
                req.set("code", 0);
                req.set("msg", "工装列表导出成功");
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                req.set("code", 1);
                req.set("msg", "失败！");
                return false;
            }
        });
        renderJson(req);
    }

    public void downFrockExcel() {
        Record rd = new Record();
        try {
            renderFile("/moban/工装列表.xlsx");
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无！");
            renderJson(rd);
        }
    }

    public List<FrockExportModel> getDataList(List<Frocks> frockList) {
        List<FrockExportModel> dataList = new ArrayList<FrockExportModel>(frockList.size());
        for (int i = 0; i < frockList.size(); i++) {
            int status = frockList.get(i).getStatus();
            String statusName = "";
            if (status == 0) {
                statusName = "可使用";
            } else if (status == 1) {
                statusName = "封存";
            } else {
                statusName = "待修";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            FrockExportModel exportModel = new FrockExportModel();
            exportModel.setBarcode(frockList.get(i).getBarcode());
            exportModel.setFrock_name(frockList.get(i).getFrockName());
            exportModel.setFrock_barcode(frockList.get(i).getFrockBarcode());
            exportModel.setStatus(statusName);
            exportModel.setProduct(frockList.get(i).getProduct());
            exportModel.setProduce(frockList.get(i).getProduce());
            exportModel.setProduce_name(frockList.get(i).getProduceName());
            exportModel.setTotal_num(String.valueOf(frockList.get(i).getTotalNum()));
            exportModel.setInstore_num(String.valueOf(frockList.get(i).getInstoreNum()));
            exportModel.setUnit(frockList.get(i).getUnit());
            exportModel.setTech_user_name(frockList.get(i).getTechUserName());
            exportModel.setArrival_date(sdf.format(frockList.get(i).getArrivalDate()));
            exportModel.setLocation(frockList.get(i).getLocation());
            exportModel.setNext_check_date(sdf.format(frockList.get(i).getNextCheckDate()));
            exportModel.setCreate_user_name(frockList.get(i).getCreateUserName());
            exportModel.setIfs_barcode(frockList.get(i).get("ifs_part_no"));
            dataList.add(exportModel);
        }
        return dataList;
    }
}
