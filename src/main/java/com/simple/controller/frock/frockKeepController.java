package com.simple.controller.frock;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Workbook;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.excelModel.KeepExportModel;
/**
 * 工装保养记录keepPc
 * @author FL00024996
 *
 */
public class frockKeepController extends Controller {
	//指向所有保养记录页面
	public void toKeepList() {
		render("keepList.html");
	}
	public void getKeepList() {
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from frocks a,keep_record b where a.id=b.frock_id ");

		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sb.append(" and a.barcode like '%"+barcode+"%' ");
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sb.append(" and a.frock_name like '%"+frockName+"%' ");
		}
		String frockBarcode=get("frockBarcode");
		if (!"".equals(frockBarcode)&&frockBarcode!=null) {
			sb.append(" and a.frock_barcode like '%"+frockBarcode+"%' ");
		}
		
		String keepUser=get("keepUser");
		if (keepUser!=null&&!"".equals(keepUser)) {
			sb.append(" and b.keep_user_name like '%"+keepUser+"%' ");
		}
		
		Page<Record> receiveList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.barcode,a.frock_name,a.frock_barcode ",sb.toString()+" order by b.id desc ");
		Record jsp=new Record();
		jsp.set("count", receiveList.getTotalRow());
		jsp.set("data", receiveList.getList());
		jsp.set("msg", "获取保养列表成功");
		renderJson(jsp);
	}
	//导出
	public void exportKeep() {
		String sqlKeep="select b.*,a.barcode,a.frock_name,a.frock_barcode from frocks a,keep_record b where a.id=b.frock_id ";		
		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sqlKeep=sqlKeep+" and a.barcode like '%"+barcode+"%' ";
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sqlKeep=sqlKeep+" and a.frock_name like '%"+frockName+"%' ";
		}
		String keepUser=get("keepUser");
		if (keepUser!=null&&!"".equals(keepUser)) {
			sqlKeep=sqlKeep+" and b.keep_user_name like '%"+keepUser+"%' ";
		}
		List<Record> keepList=Db.use("dc").find(sqlKeep);
		Record req=new Record();
		Db.tx(() -> {
			try {
				List<KeepExportModel> dataList = this.getDataList(keepList);
				Workbook workbook = DefaultExcelBuilder.of(KeepExportModel.class)
				         .build(dataList);
				FileExportUtil.export(workbook, new File("D:/FrockFujian/upload/moban/保养记录.xlsx"));
				req.set("code", 0);
				req.set("msg", "保养记录导出成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	public void downKeepExcel() {
		Record rd=new Record();
		try {
			renderFile("/moban/保养记录.xlsx");
		} catch (Exception e) {
			rd.set("code", 1);
			rd.set("msg", "暂无！");
			renderJson(rd);
		}
	}
	public List<KeepExportModel> getDataList(List<Record> frockList) {
		List<KeepExportModel> dataList=new ArrayList<KeepExportModel>(frockList.size());
	    for (int i = 0; i < frockList.size(); i++) {
	    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    	KeepExportModel exportModel=new KeepExportModel();
	    	exportModel.setBarcode(frockList.get(i).getStr("barcode"));
	    	exportModel.setFrock_name(frockList.get(i).getStr("frock_name"));
	    	exportModel.setFrock_barcode(frockList.get(i).getStr("frock_barcode"));
	    	exportModel.setKeep_user_name(frockList.get(i).getStr("keep_user_name"));
	    	exportModel.setKeep_week("第"+frockList.get(i).getStr("keep_week")+"周");
	    	exportModel.setKeep_date(sdf.format(frockList.get(i).getDate("keep_date")));
	    	exportModel.setKeep_remarks(frockList.get(i).getStr("keep_remarks"));
	    	dataList.add(exportModel);
	    }
	    return dataList;
	}
}
