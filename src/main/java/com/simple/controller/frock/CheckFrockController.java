package com.simple.controller.frock;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Workbook;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.CheckRecord;
import com.simple.common.model.Frocks;
import com.simple.excelModel.CheckExportModel;
/**
 * 工装校验check
 * @author FL00024996
 *
 */
public class CheckFrockController extends Controller {
	@SuppressWarnings("rawtypes")
	public void doCheck() {
		try {
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			String fileList=get("fileList");
			JSONArray fileArray=JSONArray.parseArray(fileList);
			JSONObject fileObject=fileArray.getJSONObject(0);
			long fileId=fileObject.getLongValue("id");
			long frockCheckId=getParaToLong("frockCheckId");
			
			int checkResultIndex=Integer.valueOf(map.get("result").toString());
			int checkCycle=Integer.valueOf(map.get("check_cycle").toString());
			//校验表新增数据
			Date date=new Date(); 
			CheckRecord checkRecord=getModel(CheckRecord.class);
			checkRecord.setFrockId(frockCheckId)
			.setCheckUserId(user.getUserid()).setCheckUserName(user.getName())
			.setCheckResult(checkResultIndex)
			.setCheckDate(date)
			.setCheckCycle(checkCycle)
			.setFujianId(fileId)
			.setCheckRemarks(map.get("check_cycle").toString());
			checkRecord.save();
			
			int status=Integer.valueOf(map.get("status").toString());
			//获取工装当前有效期，得到新有效期
			Frocks frock=Frocks.dao.findById(frockCheckId);
			Calendar calendar=Calendar.getInstance();
			calendar.clear();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, checkCycle);
			Date newYouxiaoqiDate=calendar.getTime();
			frock.setNextCheckDate(newYouxiaoqiDate)
			.setStatus(status);
			frock.update();
			
			renderJson(Ret.ok("msg", "新增成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//指向所有校验记录列表
	public void toCheckList() {
		render("checkList.html");
	}
	public void getCheckList() {
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from frocks a,check_record b where a.id=b.frock_id ");
		
		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sb.append(" and a.barcode like '%"+barcode+"%' ");
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sb.append(" and a.frock_name like '%"+frockName+"%' ");
		}
		String frockBarcode=get("frockBarcode");
		if (!"".equals(frockBarcode)&&frockBarcode!=null) {
			sb.append(" and a.frock_barcode like '%"+frockBarcode+"%' ");
		}
		String checkUser=get("checkUser");
		if (checkUser!=null&&!"".equals(checkUser)) {
			sb.append(" and b.check_user_name like '%"+checkUser+"%' ");
		}
		String checkStatus=get("checkStatus");
		if (checkStatus!=null&&!"".equals(checkStatus)) {
			int result=Integer.parseInt(checkStatus);
			sb.append(" and b.check_result="+result);
		}
		
		Page<Record> receiveList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.barcode,a.frock_name,a.frock_barcode,a.next_check_date ",sb.toString()+" order by b.id desc ");
		Record jsp=new Record();
		jsp.set("count", receiveList.getTotalRow());
		jsp.set("data", receiveList.getList());
		jsp.set("msg", "获取校验列表成功");
		renderJson(jsp);
	}
	//导出
	public void exportCheck() {
		String sqlCheck="select b.*,a.barcode,a.frock_name,a.frock_barcode,a.next_check_date from frocks a,check_record b where a.id=b.frock_id ";		
		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sqlCheck=sqlCheck+" and a.barcode like '%"+barcode+"%' ";
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sqlCheck=sqlCheck+" and a.frock_name like '%"+frockName+"%' ";
		}
		String checkUser=get("checkUser");
		if (checkUser!=null&&!"".equals(checkUser)) {
			sqlCheck=sqlCheck+" and b.check_user_name like '%"+checkUser+"%' ";
		}
		String checkStatus=get("checkStatus");
		if (checkStatus!=null&&!"".equals(checkStatus)) {
			int result=Integer.parseInt(checkStatus);
			sqlCheck=sqlCheck+" and b.check_result="+result;
		}
		List<Record> checkList=Db.use("dc").find(sqlCheck);
		Record req=new Record();
		Db.tx(() -> {
			try {
				List<CheckExportModel> dataList = this.getDataList(checkList);
				Workbook workbook = DefaultExcelBuilder.of(CheckExportModel.class)
				         .build(dataList);
				FileExportUtil.export(workbook, new File("D:/FrockFujian/upload/moban/校验记录.xlsx"));
				req.set("code", 0);
				req.set("msg", "校验记录导出成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	public void downCheckExcel() {
		Record rd=new Record();
		try {
			renderFile("/moban/校验记录.xlsx");
		} catch (Exception e) {
			rd.set("code", 1);
			rd.set("msg", "暂无！");
			renderJson(rd);
		}
	}
	public List<CheckExportModel> getDataList(List<Record> frockList) {
		List<CheckExportModel> dataList=new ArrayList<CheckExportModel>(frockList.size());
	    for (int i = 0; i < frockList.size(); i++) {
	    	int status=frockList.get(i).getInt("check_result");
	    	String statusName="";
	    	if (status==0) {
	    		statusName="不合格";
			}else {
				statusName="合格";
			}
	    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    	CheckExportModel exportModel=new CheckExportModel();
	    	exportModel.setBarcode(frockList.get(i).getStr("barcode"));
	    	exportModel.setFrock_name(frockList.get(i).getStr("frock_name"));
	    	exportModel.setFrock_barcode(frockList.get(i).getStr("frock_barcode"));
	    	exportModel.setCheck_user_name(frockList.get(i).getStr("check_user_name"));
	    	exportModel.setCheck_result(statusName);
	    	exportModel.setCheck_date(sdf.format(frockList.get(i).getDate("check_date")));
	    	exportModel.setCheck_cycle(frockList.get(i).getStr("check_cycle")+"天");
	    	exportModel.setCext_check_date(sdf.format(frockList.get(i).getDate("next_check_date")));
	    	exportModel.setCheck_remarks(frockList.get(i).getStr("check_remarks"));
	    	dataList.add(exportModel);
	    }
	    return dataList;
	}
}
