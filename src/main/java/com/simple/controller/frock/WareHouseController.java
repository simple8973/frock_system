package com.simple.controller.frock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.UserRole;
import com.simple.common.model.UserWare;
import com.simple.common.model.WareHouse;
import com.simple.common.model.WareHouseRecord;
/**
 * 车间 wareHouse
 * @author FL00024996
 *
 */
public class WareHouseController extends Controller {
	//指向车间页面
	public void toWareHouse() {
		render("wareHouse.html");
	}
	//获取一开始的页面信息
	public void getWareHouse() {
		String wareName=get("wareName");
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from ware_house  where 1=1 ");
		if (!"".equals(wareName)&&wareName!=null) {
			sb.append(" and ware_name like '%"+wareName+"%' ");
		}
		Page<Record> wareList=Db.use("dc").paginate(currentPage, pageSize, "select * ",sb.toString());
		for (int i = 0; i < wareList.getList().size(); i++) {
			//车间下工装管理员
			List<UserWare> wareUser=UserWare.dao.find("select * from user_ware where ware_id="+wareList.getList().get(i).getInt("id")+" and status=0");
			List<String> userIds=new ArrayList<String>();
			for (int j = 0; j < wareUser.size(); j++) {
				userIds.add(wareUser.get(j).getUserId());
			}
			wareList.getList().get(i).set("frock_managers", userIds);
		}
		Record jsp=new Record();
		jsp.set("count", wareList.getTotalRow());
		jsp.set("data", wareList.getList());
		//工装管理员列表
		List<UserRole> frockManagerList=UserRole.dao.find("select * from user_role where role_id=1");
		List<Record> frockManagers=new ArrayList<Record>();
		for (int i = 0; i < frockManagerList.size(); i++) {
			Record manager=new Record();
			manager.set("label", frockManagerList.get(i).getUserName()).set("value", frockManagerList.get(i).getUserId());
			frockManagers.add(manager);
		}
		jsp.set("frockManagerList", frockManagers);
		jsp.set("msg", "获取成功");
		renderJson(jsp);
	}
	//新增车间的保存
	@SuppressWarnings("rawtypes")
	public void doAddWare() {
		try {
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			String wareName=map.get("ware_name").toString();
			WareHouse wareHouse=getModel(WareHouse.class);
			wareHouse.setWareName(wareName);
			wareHouse.save();
			//车间管理员
			JSONArray managerIds=JSONArray.parseArray(map.get("frock_managers").toString());
			for (int i = 0; i < managerIds.size(); i++) {
				UserWare wareUser=new UserWare();
				wareUser.setUserId(managerIds.getString(i)).setWareId(wareHouse.getId()).setStatus(0);
				wareUser.save();
			}
			//记录表
			WareHouseRecord houseRecord=new WareHouseRecord();
			houseRecord.setWareId(wareHouse.getId()).setContent(map.get("frock_managers").toString())
			.setUserId(user.getUserid()).setCreateTime(new Date());
			houseRecord.save();
			
			renderJson(Ret.ok("msg", "新增成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//保存编辑
	@SuppressWarnings("rawtypes")
	public void doEditWare() {
		try {
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			int wareId=getParaToInt("wareId");
			WareHouse wareHouse=WareHouse.dao.findById(wareId);
			wareHouse.setWareName(map.get("ware_name").toString());
			wareHouse.update();
			//车间管理员
			String updateSql="update user_ware set status=1 where ware_id="+wareId;
			Db.use("dc").update(updateSql);
			JSONArray managerIds=JSONArray.parseArray(map.get("frock_managers").toString());
			for (int i = 0; i < managerIds.size(); i++) {
				UserWare wareUser=UserWare.dao.findFirst("select * from user_ware where ware_id="+wareId+" and user_id='"+managerIds.getString(i)+"'");
				if (wareUser==null) {
					wareUser=new UserWare();
					wareUser.setUserId(managerIds.getString(i)).setWareId(wareHouse.getId()).setStatus(0);
					wareUser.save();
				}else {
					wareUser.setStatus(0);
					wareUser.update();
				}
			}
			//记录表
			WareHouseRecord houseRecord=new WareHouseRecord();
			houseRecord.setWareId(wareHouse.getId()).setContent(map.get("frock_managers").toString())
			.setUserId(user.getUserid()).setCreateTime(new Date());
			houseRecord.save();
			renderJson(Ret.ok("msg", "编辑成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
}
