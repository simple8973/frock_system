package com.simple.controller.frock;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import org.apache.poi.ss.usermodel.Workbook;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.Frocks;
import com.simple.common.model.ReceiveBack;
import com.simple.ding.DingUtil;
import com.simple.excelModel.ReceiveExportModel;
/**
 * 工装领用归还receive
 * @author FL00024996
 *
 */
public class ReceiveController extends Controller {
	//指向工装领用归还页面
	public void toReceiveList() {
		render("receiveList.html");
	}
	public void getReceiveList() throws ParseException {
		int currentPage=getParaToInt("currentPage");
		int pageSize=getParaToInt("pageSize");
		StringBuilder sb=new StringBuilder();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		//角色
		String sqlRole="select * from user_role where user_id='"+user.getUserid()+"' and role_id=2";
		List<Record> roleList=Db.use("dc").find(sqlRole);
		int isAdmin=0;
		if (roleList.size()>0) {
			isAdmin=1;
		}
		if (isAdmin==1) {//超级管理员			
			sb.append(" from frocks a,receive_back b where a.id=b.frock_id ");
		}else {
			sb.append(" from frocks a,receive_back b where a.id=b.frock_id and (b.receive_sp_user_id='"+user.getUserid()+"' or b.back_sp_user_id='"+user.getUserid()+"') ");
		}
		
		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sb.append(" and a.barcode like '%"+barcode+"%' ");
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sb.append(" and a.frock_name like '%"+frockName+"%' ");
		}
		String frockBarcode=get("frockBarcode");
		if (!"".equals(frockBarcode)&&frockBarcode!=null) {
			sb.append(" and a.frock_barcode like '%"+frockBarcode+"%' ");
		}
		
		String receiveUser=get("receiveUser");
		if (receiveUser!=null&&!"".equals(receiveUser)) {
			sb.append(" and b.receive_user_name like '%"+receiveUser+"%' ");
		}
		String backUser=get("backUser");
		if (backUser!=null&&!"".equals(backUser)) {
			sb.append(" and b.back_user_name like '%"+backUser+"%' ");
		}
		String receiveStatus=get("receiveStatus"); 
		if (receiveStatus!=null&&!"".equals(receiveStatus)) {
			int status=getParaToInt("receiveStatus");
			if (status!=10) {
				sb.append(" and b.status="+status);
			}else {
				sb.append(" and b.status<>9 ");
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//添加一个日期选择功能
		String selectDate = get("selectDate");
		if(selectDate!=null&&!"".equals(selectDate)&&!"null".equals(selectDate)){
			JSONArray selectDateAry = JSONArray.parseArray(selectDate);
			//时间的范围（现在都要少一天？？？？？）
			Calendar ca = Calendar.getInstance();
			Date startDate = sdf.parse(selectDateAry.get(0).toString());
			ca.setTime(startDate);
			ca.add(Calendar.DATE, 1);// 1为增加的天数，可以改变的
			startDate=ca.getTime();
			String start = sdf.format(startDate);

			Date endDate = sdf.parse(selectDateAry.get(1).toString());
			ca.setTime(endDate);
			ca.add(Calendar.DATE, 1);// 1为增加的天数，可以改变的
			endDate=ca.getTime();
			String end = sdf.format(endDate);
			sb.append(" and b.receive_date BETWEEN '"+start+"' and '"+end+"'");
		}

		Page<Record> receiveList=Db.use("dc").paginate(currentPage, pageSize, "select b.*,a.barcode,a.frock_name,a.frock_barcode ",sb.toString()+" order by b.id desc ");
		Record jsp=new Record();
		jsp.set("count", receiveList.getTotalRow());
		jsp.set("data", receiveList.getList());
		jsp.set("msg", "获取工装列表成功");
		jsp.set("loginuser", user.getUserid());
		renderJson(jsp);
	}
	//领用审批通过（前端调用该方法）
	public void passReceive() {
		try {
			long receiveId=getParaToLong("id");
			ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
			if (receive.getStatus()==0) {
				receive.setStatus(1);
				receive.update();
				Frocks frock=Frocks.dao.findById(receive.getFrockId());
				Date date=new Date();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DingUtil.sendText(receive.getReceiveUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】领用，已审批通过，请知悉！"+sdf.format(date));
				renderJson(Ret.ok("msg", "审批成功！"));
			}else {
				renderJson(Ret.fail("error","请勿重复操作！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//领用驳回
	public void bohuiReceive() {
		try {
			long receiveId=getParaToLong("id");
			ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
			if (receive.getStatus()==0) {
				receive.setStatus(2);
				receive.update();
				Frocks frock=Frocks.dao.findById(receive.getFrockId());
				//工装表在库数量增加
				int instoreNum=frock.getInstoreNum();
				int receiveNum=receive.getReceiveNum();
				int newInstoreNum=instoreNum+receiveNum;
				frock.setInstoreNum(newInstoreNum);
				frock.update();
				Date date=new Date();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DingUtil.sendText(receive.getReceiveUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】领用，被审批驳回，请知悉！"+sdf.format(date));
				renderJson(Ret.ok("msg", "审批成功！"));
			}else {
				renderJson(Ret.fail("error","请勿重复操作！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	//审批通过归还
	public void passBack() {
		try {
			long receiveId=getParaToLong("id");
			ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
			if (receive.getStatus()==3) {
				receive.setStatus(4);
				receive.update();
				long usedLife=receive.getUsedLife();
				//基本信息表在库数量增加
				Frocks frock=Frocks.dao.findById(receive.getFrockId());
				long oldUsedLife=frock.getUsedLife();
				long newUsedLife=usedLife+oldUsedLife;//新的使用寿命
				int instoreNum=frock.getInstoreNum();
				int receiveNum=receive.getReceiveNum();
				int newInstoreNum=instoreNum+receiveNum;
				frock.setInstoreNum(newInstoreNum)
				.setUsedLife(newUsedLife);;
				frock.update();
				
				Date date=new Date();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DingUtil.sendText(receive.getBackUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】归还，已审批通过，请知悉！"+sdf.format(date));
				renderJson(Ret.ok("msg", "审批成功！"));
			}else {
				renderJson(Ret.fail("error","请勿重复操作！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	public void bohuiBack() {
		try {
			long receiveId=getParaToLong("id");
			ReceiveBack receive=ReceiveBack.dao.findById(receiveId);
			if (receive.getStatus()==3) {
				receive.setStatus(1);
				receive.update();
				Frocks frock=Frocks.dao.findById(receive.getFrockId());
				
				Date date=new Date();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DingUtil.sendText(receive.getBackUserId(), "工装领用通知：你提交的工装【"+frock.getFrockName()+"】归还，被审批驳回，请知悉！"+sdf.format(date));
				renderJson(Ret.ok("msg", "审批成功！"));
			}else {
				renderJson(Ret.fail("error","请勿重复操作！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("error",e));
		}
	}
	/**
	 * 导出
	 */
	public void exportReceive() throws ParseException {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlReceive="select b.*,a.barcode,a.frock_name,a.frock_barcode from  frocks a,receive_back b "
				+ "where a.id=b.frock_id  ";
        List<Record> adminList = Db.use("dc").find("select * from user_role where user_id='" + user.getUserid() + "' and (role_id=2 or role_id=4)");
        if (adminList.isEmpty()) {
        	sqlReceive=sqlReceive+"and (b.receive_sp_user_id='"+user.getUserid()+"' or b.back_sp_user_id='"+user.getUserid()+"')";
		}
		String barcode=get("barcode");
		if (barcode!=null&&!"".equals(barcode)) {
			sqlReceive=sqlReceive+" and a.barcode like '%"+barcode+"%' ";
		}
		String frockName=get("frockName");
		if (frockName!=null&&!"".equals(frockName)) {
			sqlReceive=sqlReceive+" and a.frock_name like '%"+frockName+"%' ";
		}
		String receiveUser=get("receiveUser");
		if (receiveUser!=null&&!"".equals(receiveUser)) {
			sqlReceive=sqlReceive+" and b.receive_user_name like '%"+receiveUser+"%' ";
		}
		String backUser=get("backUser");
		if (backUser!=null&&!"".equals(backUser)) {
			sqlReceive=sqlReceive+" and b.back_user_name like '%"+backUser+"%' ";
		}
		String receiveStatus=get("receiveStatus"); 
		if (receiveStatus!=null&&!"".equals(receiveStatus)) {
			int status=getParaToInt("receiveStatus");
			if (status!=10) {
				sqlReceive=sqlReceive+" and b.status="+status;
			}else {
				sqlReceive=sqlReceive+" and b.status<>9 ";
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//添加一个日期选择功能
		String selectDate = get("selectDate");
		if(selectDate!=null&&!"".equals(selectDate)&&!"null".equals(selectDate)){
			JSONArray selectDateAry = JSONArray.parseArray(selectDate);
			//时间的范围（现在都要少一天，只能手动增加一天）
			Calendar ca = Calendar.getInstance();
			Date startDate = sdf.parse(selectDateAry.get(0).toString());
			ca.setTime(startDate);
			ca.add(Calendar.DATE, 1);// 1为增加的天数，可以改变的
			startDate=ca.getTime();
			String start = sdf.format(startDate);

			Date endDate = sdf.parse(selectDateAry.get(1).toString());
			ca.setTime(endDate);
			ca.add(Calendar.DATE, 1);// 1为增加的天数，可以改变的
			endDate=ca.getTime();
			String end = sdf.format(endDate);
			sqlReceive=sqlReceive+" and b.receive_date BETWEEN '"+start+"' and '"+end+"'";
		}
		
		List<Record> ReceiveList=Db.use("dc").find(sqlReceive);
		Record req=new Record();
		Db.tx(() -> {
			try {
				List<ReceiveExportModel> dataList = this.getDataList(ReceiveList);
				Workbook workbook = DefaultExcelBuilder.of(ReceiveExportModel.class)
				         .build(dataList);
				FileExportUtil.export(workbook, new File("D:/FrockFujian/upload/moban/领用归还记录.xlsx"));
				req.set("code", 0);
				req.set("msg", "校验记录导出成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	public void downReceiveExcel() {
		Record rd=new Record();
		try {
			renderFile("/moban/领用归还记录.xlsx");
		} catch (Exception e) {
			rd.set("code", 1);
			rd.set("msg", "暂无！");
			renderJson(rd);
		}
	}
	public List<ReceiveExportModel> getDataList(List<Record> frockList) {
		List<ReceiveExportModel> dataList=new ArrayList<ReceiveExportModel>(frockList.size());
	    for (int i = 0; i < frockList.size(); i++) {
	    	int status=frockList.get(i).getInt("status");
	    	String statusName="";
	    	if (status==0) {
	    		statusName="领用待审批";
			}else if (status==1) {
	    		statusName="领用中";
			}else if (status==2) {
	    		statusName="领用驳回";
			}else if (status==3) {
	    		statusName="归还待审批";
			}else {
				statusName="已归还";
			}
	    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    	ReceiveExportModel exportModel=new ReceiveExportModel();
	    	exportModel.setBarcode(frockList.get(i).getStr("barcode"));
	    	exportModel.setFrock_name(frockList.get(i).getStr("frock_name"));
	    	exportModel.setFrock_barcode(frockList.get(i).getStr("frock_barcode"));
	    	exportModel.setReceive_user_name(frockList.get(i).getStr("receive_user_name"));
	    	exportModel.setReceive_num(frockList.get(i).getStr("receive_num"));
	    	exportModel.setReceive_date(sdf.format(frockList.get(i).getDate("receive_date")));
	    	exportModel.setStatus(statusName);
	    	Date back_date=frockList.get(i).getDate("back_date");
	    	if (back_date!=null&&!"null".equals(back_date)) {
	    		exportModel.setBack_date(sdf.format(frockList.get(i).getDate("back_date")));
			}
	    	
	    	exportModel.setBack_user_name(frockList.get(i).getStr("back_user_name"));
	    	dataList.add(exportModel);
	    }
	    return dataList;
	}
}
