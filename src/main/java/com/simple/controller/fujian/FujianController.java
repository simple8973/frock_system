package com.simple.controller.fujian;

import java.util.Date;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.simple.common.model.Fujians;
/**
 * 附件上传 下载 fujian
 * @author FL00024996
 *
 */
public class FujianController extends Controller {
	//上传
	public void upload() {
		Record resp = new Record();
		Date date=new Date();
		try {
			UploadFile file = getFile("file", "");
			String fileName=file.getFileName();
			Fujians fujian=getModel(Fujians.class);
			fujian.setFujianName(fileName).setCreateTime(date);
			fujian.save();
			resp.set("code", 0);
			resp.set("fileid", fujian.getId());
			resp.set("filename", fileName);
			resp.set("msg", "上传成功！");
		} catch (Exception e) {
			resp.set("code", 1);
			resp.set("msg", "上传失败，原因："+e.getMessage());
			e.printStackTrace();
		}
    	renderJson(resp);
	}
	//附件下载
	public void downLoad() {
		Record rd=new Record();
		long fujianId=getParaToLong(0);
		Fujians fujian=Fujians.dao.findById(fujianId);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
}
