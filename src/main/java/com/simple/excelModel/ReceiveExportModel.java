package com.simple.excelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

/**
 * 领用导出列表
 * @author FL00024996
 *
 */
public class ReceiveExportModel {
	@ExcelColumn(order = 1, title = "工装编号")
    private String barcode;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode=barcode;
	}
	@ExcelColumn(order = 2, title = "工装名称")
    private String frock_name;
	public String getFrock_name() {
		return frock_name;
	}
	public void setFrock_name(String frock_name) {
		this.frock_name=frock_name;
	}
	@ExcelColumn(order = 3, title = "工装图号")
    private String frock_barcode;
	public String getFrock_barcode() {
		return frock_barcode;
	}
	public void setFrock_barcode(String frock_barcode) {
		this.frock_barcode=frock_barcode;
	}
	@ExcelColumn(order = 4, title = "领用人")
    private String receive_user_name;
	public String getReceive_user_name() {
		return receive_user_name;
	}
	public void setReceive_user_name(String receive_user_name) {
		this.receive_user_name=receive_user_name;
	}
	@ExcelColumn(order = 5, title = "领用数量")
    private String receive_num;
	public String getReceive_num() {
		return receive_num;
	}
	public void setReceive_num(String receive_num) {
		this.receive_num=receive_num;
	}
	@ExcelColumn(order = 6, title = "领用日期")
    private String receive_date;
	public String getReceive_date() {
		return receive_date;
	}
	public void setReceive_date(String receive_date) {
		this.receive_date=receive_date;
	}
	@ExcelColumn(order = 7, title = "状态")
    private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status=status;
	}
	@ExcelColumn(order = 8, title = "归还人")
    private String back_user_name;
	public String getBack_user_name() {
		return back_user_name;
	}
	public void setBack_user_name(String back_user_name) {
		this.back_user_name=back_user_name;
	}
	@ExcelColumn(order = 9, title = "归还日期")
    private String back_date;
	public String getBack_date() {
		return back_date;
	}
	public void setBack_date(String back_date) {
		this.back_date=back_date;
	}
}
