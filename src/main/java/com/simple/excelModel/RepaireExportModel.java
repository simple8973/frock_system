package com.simple.excelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

/**
 * 维修记录
 * @author FL00024996
 *
 */
public class RepaireExportModel {
	@ExcelColumn(order = 1, title = "工装编号")
    private String barcode;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode=barcode;
	}
	@ExcelColumn(order = 2, title = "工装名称")
    private String frock_name;
	public String getFrock_name() {
		return frock_name;
	}
	public void setFrock_name(String frock_name) {
		this.frock_name=frock_name;
	}
	@ExcelColumn(order = 3, title = "工装图号")
    private String frock_barcode;
	public String getFrock_barcode() {
		return frock_barcode;
	}
	public void setFrock_barcode(String frock_barcode) {
		this.frock_barcode=frock_barcode;
	}
	@ExcelColumn(order = 4, title = "申请人")
    private String sq_user_name;
	public String getSq_user_name() {
		return sq_user_name;
	}
	public void setSq_user_name(String sq_user_name) {
		this.sq_user_name=sq_user_name;
	}
	@ExcelColumn(order = 5, title = "报修日期")
    private String sq_date;
	public String getSq_date() {
		return sq_date;
	}
	public void setSq_date(String sq_date) {
		this.sq_date=sq_date;
	}
	@ExcelColumn(order = 6, title = "问题描述")
    private String problem;
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem=problem;
	}
	@ExcelColumn(order = 7, title = "状态")
    private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status=status;
	}
	@ExcelColumn(order = 8, title = "审批人")
    private String back_user_name;
	public String getBack_user_name() {
		return back_user_name;
	}
	public void setBack_user_name(String back_user_name) {
		this.back_user_name=back_user_name;
	}
	@ExcelColumn(order = 9, title = "开始维修日期")
    private String hand_date;
	public String getHand_date() {
		return hand_date;
	}
	public void setHand_date(String hand_date) {
		this.hand_date=hand_date;
	}
	@ExcelColumn(order = 10, title = "故障原因")
    private String reason;
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason=reason;
	}
	@ExcelColumn(order = 11, title = "处理意见")
    private String resolution;
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution=resolution;
	}
	@ExcelColumn(order = 12, title = "维修完成日期")
    private String finish_date;
	public String getFinish_date() {
		return finish_date;
	}
	public void setFinish_date(String finish_date) {
		this.finish_date=finish_date;
	}
}
