package com.simple.excelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

/**
 * 工装列表导出
 * @author FL00024996
 *
 */
public class FrockExportModel {
	@ExcelColumn(order = 1, title = "工装编号")
    private String barcode;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode=barcode;
	}
	@ExcelColumn(order = 2, title = "工装名称")
    private String frock_name;
	public String getFrock_name() {
		return frock_name;
	}
	public void setFrock_name(String frock_name) {
		this.frock_name=frock_name;
	}
	@ExcelColumn(order = 3, title = "工装图号")
    private String frock_barcode;
	public String getFrock_barcode() {
		return frock_barcode;
	}
	public void setFrock_barcode(String frock_barcode) {
		this.frock_barcode=frock_barcode;
	}
	@ExcelColumn(order = 4, title = "状态")
    private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status=status;
	}
	@ExcelColumn(order = 5, title = "使用产品")
    private String product;
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product=product;
	}
	@ExcelColumn(order = 6, title = "工站序号")
    private String produce;
	public String getProduce() {
		return produce;
	}
	public void setProduce(String produce) {
		this.produce=produce;
	}
	@ExcelColumn(order = 7, title = "工站名称")
    private String produce_name;
	public String getProduce_name() {
		return produce_name;
	}
	public void setProduce_name(String produce_name) {
		this.produce_name=produce_name;
	}
	@ExcelColumn(order = 8, title = "总数量")
    private String total_num;
	public String getTotal_num() {
		return total_num;
	}
	public void setTotal_num(String total_num) {
		this.total_num=total_num;
	}
	@ExcelColumn(order = 9, title = "库存数量")
    private String instore_num;
	public String getInstore_num() {
		return instore_num;
	}
	public void setInstore_num(String instore_num) {
		this.instore_num=instore_num;
	}
	@ExcelColumn(order = 10, title = "单位")
    private String unit;
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit=unit;
	}
	@ExcelColumn(order = 11, title = "技术员")
    private String tech_user_name;
	public String getTech_user_name() {
		return tech_user_name;
	}
	public void setTech_user_name(String tech_user_name) {
		this.tech_user_name=tech_user_name;
	}
	@ExcelColumn(order = 12, title = "入厂日期")
    private String arrival_date;
	public String getArrival_date() {
		return arrival_date;
	}
	public void setArrival_date(String arrival_date) {
		this.arrival_date=arrival_date;
	}
	@ExcelColumn(order = 13, title = "存放位置")
    private String location;
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location=location;
	}
	@ExcelColumn(order = 14, title = "有效期")
    private String next_check_date;
	public String getNext_check_date() {
		return next_check_date;
	}
	public void setNext_check_date(String next_check_date) {
		this.next_check_date=next_check_date;
	}
	@ExcelColumn(order = 15, title = "创建人")
    private String create_user_name;
	public String getCreate_user_name() {
		return create_user_name;
	}
	public void setCreate_user_name(String create_user_name) {
		this.create_user_name=create_user_name;
	}
	@ExcelColumn(order = 16, title = "IFS编码")
    private String ifs_barcode;
	public String getIfs_barcode() {
		return ifs_barcode;
	}
	public void setIfs_barcode(String ifs_barcode) {
		this.ifs_barcode=ifs_barcode;
	}
}
