package com.simple.excelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

/**
 * 校验记录导出
 * @author FL00024996
 *
 */
public class CheckExportModel {
	@ExcelColumn(order = 1, title = "工装编号")
    private String barcode;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode=barcode;
	}
	@ExcelColumn(order = 2, title = "工装名称")
    private String frock_name;
	public String getFrock_name() {
		return frock_name;
	}
	public void setFrock_name(String frock_name) {
		this.frock_name=frock_name;
	}
	@ExcelColumn(order = 3, title = "工装图号")
    private String frock_barcode;
	public String getFrock_barcode() {
		return frock_barcode;
	}
	public void setFrock_barcode(String frock_barcode) {
		this.frock_barcode=frock_barcode;
	}
	@ExcelColumn(order = 4, title = "校验人")
    private String check_user_name;
	public String getCheck_user_name() {
		return check_user_name;
	}
	public void setCheck_user_name(String check_user_name) {
		this.check_user_name=check_user_name;
	}
	@ExcelColumn(order = 5, title = "校验结果")
    private String check_result;
	public String getCheck_result() {
		return check_result;
	}
	public void setCheck_result(String check_result) {
		this.check_result=check_result;
	}
	@ExcelColumn(order = 6, title = "校验日期")
    private String check_date;
	public String getCheck_date() {
		return check_date;
	}
	public void setCheck_date(String check_date) {
		this.check_date=check_date;
	}
	@ExcelColumn(order = 7, title = "校验周期")
    private String check_cycle;
	public String getCheck_cycle() {
		return check_cycle;
	}
	public void setCheck_cycle(String check_cycle) {
		this.check_cycle=check_cycle;
	}
	@ExcelColumn(order = 8, title = "有效期")
    private String next_check_date;
	public String getCext_check_date() {
		return next_check_date;
	}
	public void setCext_check_date(String next_check_date) {
		this.next_check_date=next_check_date;
	}
	@ExcelColumn(order = 9, title = "校验备注")
    private String check_remarks;
	public String getCheck_remarks() {
		return check_remarks;
	}
	public void setCheck_remarks(String check_remarks) {
		this.check_remarks=check_remarks;
	}
}
