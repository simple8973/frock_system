package com.simple.util;

import static com.simple.common.config.URLConstant.URL_GET_TOKEN;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.simple.common.config.Constant;
import com.taobao.api.ApiException;

public class AccessTokenUtil {
	public static final Logger bizLogger=LoggerFactory.getLogger(AccessTokenUtil.class);
	public static String getToken() throws RuntimeException{
		try {
			DefaultDingTalkClient client=new DefaultDingTalkClient(URL_GET_TOKEN);
			OapiGettokenRequest request=new OapiGettokenRequest();
			request.setAppkey(Constant.APP_KEY);
			request.setAppsecret(Constant.APP_SECRET);
			request.setHttpMethod("GET");
			OapiGettokenResponse response=client.execute(request);
			String accessToken =response.getAccessToken();
			return accessToken;
		}catch(ApiException e) {
			bizLogger.error("getAccessToken failed",e);
			throw new RuntimeException();
		}
	}

}
