package com.simple.ding;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.simple.common.config.Constant;
import com.simple.common.config.URLConstant;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class DingUtil {
	public static String sendText(String useridList,String message) {
		String accessToken = AccessTokenUtil.getToken();
		DingTalkClient client = new DefaultDingTalkClient(URLConstant.URL_MESSAGE_ASYNCSEND_V2);
		
		OapiMessageCorpconversationAsyncsendV2Request request = new OapiMessageCorpconversationAsyncsendV2Request();
		request.setUseridList(useridList);
		request.setAgentId(Constant.AGENT_ID);
		request.setToAllUser(false);
		
		OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
		msg.setOa(new OapiMessageCorpconversationAsyncsendV2Request.OA());
		msg.getOa().setHead(new OapiMessageCorpconversationAsyncsendV2Request.Head());
		msg.getOa().getHead().setText("工装管理系统");
		msg.getOa().setBody(new OapiMessageCorpconversationAsyncsendV2Request.Body());
		msg.getOa().getBody().setContent(message);
		msg.getOa().setPcMessageUrl("dingtalk://dingtalkclient/action/openapp?corpid=ding1410adc97eed08c335c2f4657eb6378f&container_type=work_platform&app_id=0_876871875&redirect_type=jump&redirect_url=http%3a%2f%2f110.186.68.166%3a1188%2fdingLogin");
		msg.setMsgtype("oa");
		request.setMsg(msg);
		
		OapiMessageCorpconversationAsyncsendV2Response response = null;
		try {
			response = client.execute(request,accessToken);
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.getBody();
	}
}
